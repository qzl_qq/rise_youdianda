$(function () {
	// 注册接口
	let layer = layui.layer

	// 手机的正则
	let phoneNumber = /^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$/
	//密码的正则
	let pwd = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$/

	// 注册 点击事件
	$('#btn-reg').on('click', function () {
		let obj = {}
		obj.mobile = $('#username').val().trim()
		obj.password = $('#password').val().trim()
		// 判断输入内容是否符合要求
		if (!phoneNumber.test(obj.mobile)) {
			return layer.msg('手机格式不正确', { icon: 5 })
		}
		if (!pwd.test(obj.password)) {
			return layer.msg('密码最短6位且必须有英文+数字', { icon: 5 })
		}
		if (obj.password !== $('#new_paw').val()) {
			return layer.msg('两次输入密码不一致', { icon: 5 })
		}
		console.log(obj)
		postInfo(obj)
	})

	// 提交注册接口
	function postInfo(data) {
		axios({
			method: 'POST',
			url: QTURL + 'home/index/reg',
			data: data,
		}).then(function (res) {
			console.log(res)
			if (res.status !== 200) {
				return layer.msg('账号已存在', { icon: 5 })
			} else {
				layer.msg('注册成功', { icon: 6 })
				setTimeout(function () {
					location.href = './login-登录.html'
				}, 2000)
			}

			// 存入本地token
			localStorage.setItem('token', res.data.data.token)
		})
	}
})
