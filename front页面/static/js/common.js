$(function () {
	$.ajaxPrefilter(function (options) {
		options.url = 'http://192.168.18.32:8060/home' + options.url
		// 全局携带token
		options.headers = { token: localStorage.getItem('token') }
		// 全局统一挂载 complete 回调函数
		options.complete = function (res) {
			if (res.responseJSON.errno !== 0 && res.responseJSON.errmsg == '未登录，不允许操作！') {
				// localStorage.removeItem('token')
				// location.href = './login.html'
			}
		}
	})

	let id = localStorage.getItem('cateId')

	function render() {
		let token = localStorage.getItem('token')
		let str = ''
		if (!token) {
			str = `<a href="login-登录.html"><i class="fa fa-hand-o-right"></i>&nbsp;登录</a>  | 
      <a href="reg-注册.html"><i class="fa fa-registered"></i>&nbsp;注册</a> `
			$('.slinks').html(str)
		} else {
			$.ajax({
				type: 'get',
				url: '/user/getUserInfo',
				success: function (res) {
					console.log(res)
					myName = res.data.userInfo.username
					str = `
          <a href="user.html"><i class="fa fa-hand-o-right"></i>&nbsp;${myName}</a>  |  <a href="javascript:;" class='logout' >退出登录</a>
          `
					$('.slinks').html(str)
				},
			})
		}
	}
	render()
	$('.slinks').on('click', 'a', function (e) {
		if ($(this).hasClass('logout')) {
			// console.log(1)
			e.preventDefault()
			localStorage.removeItem('token')
			location.href = './login-登录.html'
			render()
		}
	})
})
