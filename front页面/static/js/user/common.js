$(function () {
	let id = localStorage.getItem('cateId')
	$.ajax({
		method: 'GET',
		url: '/index/index',
		success: function (res) {
			// console.log(res.data)
			let str = res.data.allCate
				.map(function (item) {
					return `
    <li class="menu-item menu-item-type-taxonomy menu-item-object-category ${
			item.id == id ? 'current-menu-item current_page_item' : ''
		}">
    <a title="" href="javascript:;" data-id="${
			item.id
		}" ><span class="fa ${item.icon}"></span>${item.catename}</a>
    </li>
    `
				})
				.join('')
			$('.nav').append(str)
			let str2 = res.data.allLink
				.map(function (item) {
					if (item.title == '') {
						return `
          <a href="${item.url}" title="百度">${item.des}</a> <span>|</span>
          `
					}
				})
				.join('')
			$('.footer .link').append(str2)
		},
	})
	$('.nav').on('click', 'a', function (e) {
		e.preventDefault()
		let id = $(this).data('id')
		localStorage.setItem('cateId', id)
		location.href = './index-主页.html'
	})
	function render() {
		let token = localStorage.getItem('token')
		let str = ''
		if (!token) {
			str = `<a href="login-登录.html"><i class="fa fa-hand-o-right"></i>&nbsp;登录</a>  | 
      <a href="reg-注册.html"><i class="fa fa-registered"></i>&nbsp;注册</a> `
			$('.slinks').html(str)
		} else {
			$.ajax({
				type: 'get',
				url: '/user/getUserInfo',
				success: function (res) {
					myName = res.data.userInfo.username
					str = `<a href="">优惠券抢购中心</a></a>
          <a href="user.html"><i class="fa fa-hand-o-right"></i>&nbsp;${myName}</a>  |  <a href="javascript:;" class='logout' >退出登录</a>
          `
					$('.slinks').html(str)
				},
			})
		}
	}
	render()
	$('.slinks').on('click', 'a', function (e) {
		if ($(this).hasClass('logout')) {
			// console.log(1)
			e.preventDefault()
			localStorage.removeItem('token')
			location.href = './login-登录.html'
			render()
		}
	})
})
