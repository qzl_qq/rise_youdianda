var laypage = layui.laypage;
let q = {
	pagenum: 1,
	pagesize: 1,
	cate_id: '',
	state: ''
};

// 提取本地存储密钥
let token = localStorage.getItem('token')
// 作为全局变量，方便使用，存储用户文章信息
let olddata = ''
// console.log(token)
// 请求数据
//   limit, page 必要参数
function getCate(limit, page) {
	$.ajax({
		method: 'GET',
		url: '/user/myArticle',
		data: {
			limit: limit,
			page: page,
		},
		headers: {
			token: token,
		},
		success: function (res) {
			console.log(res);
			// 把文章信息存入，方便后面使用
			olddata = res.data.data
			// 渲染
			let str = res.data.data.map((item, index) => {
				return `
        <li>
          <a target="_blank" href="javascript:;">
            <span class="thumbnail">
              <img class="thumb" src=${QTURL + item.pic} />
            </span>
             <span class="text"> ${item.title} ${item.status == 2 ? '【待发布】' : '【已发布】'
					}</span>
              ${item.status == 2
						? `${item.content}</span>
            <div class="action-btn">
            <button class="btn btn-primary btn-xs" data-id=${item.id}  ${item.status == 1 ? 'disabled' : ''
						} ><i class="fa fa-edit"></i>&nbsp;修改</button>
            <button class="btn btn-success btn-xs" data-id=${item.id
						}><i class="fa fa-send"></i>&nbsp;发布</button>
            <button class="btn btn-danger btn-xs" data-id=${item.id
						}><i class="fa fa-trash"></i>&nbsp;删除</button>
          </div>`
						: `<span class="text">${item.content}</span>
            <span class="text-muted post-views"> <i class="fa fa-eye"></i> 已阅读(${item.click})</span>
                <span class="text-muted post-views"> <i class="fa thumbs-up"></i> 被点赞(${item.istop})</span>`
					}
          </a>
        </li>
        `
			})
			// 添加数据
			$('.items-01').html(str.join(''))
			// console.log(res.data.count);
			renderPage(res.data.count)
		},
	})
}
getCate()


// 分页
function renderPage(total) {
	// console.log(total);
	laypage.render({
		elem: 'test1',
		count: total,
		limit: q.pagesize,
		curr: q.pagenum,
		layout: ['count', 'limit', 'prev', 'page', 'next', 'skip'],
		limits: [1, 2, 3, 4, 5],
		jump: function (obj, first) {
			// console.log(obj.curr);
			q.pagenum = obj.curr
			q.pagesize = obj.limit


			//首次不执行
			if (!first) {
				//do something
				getCate(q.pagesize, q.pagenum)
			}
		}
	})
}

// 发布按钮
$('.list-box').on('click', '.btn-success', function (e) {
	e.preventDefault()
	let id = $(this).attr('data-id')
	// 点击按钮，获取文章信息
	let data = getData(id)
	// 发布的status=1
	data.status = '1'
	upData(data)
})

// 获取文章信息
// 获取点击的数据
function getData(id) {
	let newData = {}
	// 遍历所有的文章
	olddata.forEach((item) => {
		// 如果某个文字的id与点击的id相同
		if (item.id == id) {
			// 提取
			newData = item
		}
	})
	return newData
}

// 发布状态
function upData(data) {
	$.ajax({
		method: 'POST',
		url: '/user/editArticle',
		data: data,
		success: function (res) {
			console.log(res);
			alert('发布成功')
			// 重新渲染
			getCate()


		},
	})
}

// 删除
$('.list-box').on('click', '.btn-danger', function (e) {
	e.preventDefault()
	let id = $(this).attr('data-id')
	// 删除函数
	Delete(id)
})

function Delete(id) {
	$.ajax({
		method: 'GET',
		url: '/user/delArticle',
		data: {
			id: id,
		},
		headers: {
			token: token,
		},
		success: function (res) {
			console.log(res)
			// 重新渲染
			getCate()

		},
	})
}

// 修改
$('.list-box').on('click', '.btn-primary', function (e) {
	e.preventDefault()
	let id = $(this).attr('data-id')
	//返回添加页面，把对应的id 值传过去
	window.location.href = './user-add.html?id=' + id
})



