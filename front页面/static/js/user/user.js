$(function () {
  $.ajax({
    method: 'GET',
    url: '/user/getUserInfo',
    success: function (res) {
      // console.log(res)
      let item = res.data
      let str = `
      <div class="title-row">
        <a href="./user-edit.html" class="user-icon"><img src="${QTURL + item.userInfo.icon}" alt="" /></a>
        <div class="text">
          <h3>
          ${item.userInfo.username} <span><b>个性宣言</b>:${item.userInfo.notes ? item.userInfo.notes : ''}</span>
          </h3>
          <p>
            性别：${item.userInfo.sex ? '女' : '男'}<span> |</span> 手机号：${item.userInfo.mobile} <span>|</span>
            出生年月：${item.userInfo.birthday ? item.userInfo.birthday : ''}
          </p>
        </div>
        <a href="./user-edit.html"><i style="font-size: 20px; cursor: pointer" class="fa fa-cogs"></i></a>
      </div>
      <div class="btn-box">
        <a href="user-add.html" class="button"><i class="fa fa-plus"></i>立即发布文章</a>
        <a href="user-center.html" class="button"><i class="fa fa-heart"></i>收藏了 <b>${item.collectNum}</b>篇文章</a>
        <a href="user-zan.html" class="button"><i class="fa fa-thumbs-up"></i>点赞了 <b>${item.zanNum}</b>篇文章</a>
        <a href="user-list.html" class="button"><i class="fa fa-tasks"></i>发布了 <b>${item.articleNum}</b>篇文章</a>
      </div>
      `
      $('.user-info').html(str)
    },
  })
})
