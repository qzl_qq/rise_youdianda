$(function () {
	let imgURL = ''
	// 提前声明  全局  存储信息
	let Data = {}
	//从接口获取用户信息
	function getXinXi() {
		$.ajax({
			method: 'GET',
			url: '/user/getUserInfo',
			success: function (res) {
				// console.log(res)
				let item = res.data.userInfo
				// 把用户信息存入，方便后面使用
				Data = item
				// 从信息中获取  sex 的值  0/1 来自动判断单选框
				if (item.sex) {
					$('#nv').prop('checked', true)
					$('#nan').prop('checked', false)
				} else {
					$('#nv').prop('checked', false)
					$('#nan').prop('checked', true)
				}

				// 通过信息中  icon(图片地址后缀)  有则显示

				let img = item.icon ? QTURL + item.icon : './static/images/default.png'
				// 把信息中的对应数据添加到表单里
				$('input[name=username]').val(item.username)
				$('input[name=mobile]').val(item.mobile)
				$('input[name=birthday]').val(item.birthday)
				$('input[name=address]').val(item.address)
				$('input[name=notes]').val(item.notes)
				$('#icon').prop('src', img)
			},
		})
	}
	getXinXi()


	function xiuGai() {
		// 获取表单修改的信息数据
		// 把值给  Data
		let sex = $('input[name=sex]:checked').val()

		Data.username = $('input[name=username]').val()
		Data.mobile = $('input[name=mobile]').val()
		// 信息中的  sex 是  男/女  Data需要的值是  0/1  转换
		Data.sex = sex === '男' ? 0 : 1
		Data.birthday = $('input[name=birthday]').val()
		Data.address = $('input[name=address]').val()
		Data.notes = $('input[name=notes]').val()
		Data.icon = imgURL
		// 返回给Data
		return Data
	}





	// 上传图片
	$('#addImg').click(function (e) {
		$('#file').click()
	})
	//  change事件
	// console.log(Data);
	$('#file').on('change', (e) => {
		let files = e.target.files
		//通过  length  判断用户是否选择了图片
		if (files.length === 0) {
			console.log(Data.icon);
			$('#icon').prop('src', Data.icon)
		}

		// 选择了图片给其创建一个地址
		let newUrl = URL.createObjectURL(files[0])

		// 给图片框的  src 设置属性
		$('#icon').attr('src', newUrl)
		getImg()
	})
	function getImg() {
		let formData = new FormData()
		let file = $('#file')[0].files
		formData.append('file', file[0])
		$.ajax({
			method: 'post',
			url: '/common/upload?type=images',
			data: formData,
			// 告诉jQuery不要去设置Content-Type请求头
			contentType: false,
			// 告诉jQuery不要去处理发送的数据
			processData: false,
			success: function (res) {
				imgURL = res.data.savePath
			},
		})
	}

	// 修改资料
	$('#sub').on('click', function () {
		// 调用 表单修改 函数
		let data = xiuGai()

		$.ajax({
			method: 'POST',
			url: '/user/update',
			data: data,
			success: function (res) {
				alert(res.errmsg)
				// 跳转到首页
				window.location.href = './user.html'
			},
		})
	})

	// 取消
	$('#cancel').click(() => {
		// 把原先的用户信息给表单
		getXinXi()
		window.location.href = 'user.html'
	})
})
