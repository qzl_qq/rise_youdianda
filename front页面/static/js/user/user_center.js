$(function () {

  var laypage = layui.laypage;
  let q = {
    pagenum: 1,
    pagesize: 1,
    cate_id: '',
    state: ''
  };

  let token = localStorage.getItem('token')
  console.log(token);

  //  获取列表
  function render(type = '1', page, limit) {
    $.ajax({
      method: 'GET',
      url: '/user/userDataList',
      data: {
        type: type,
        page: page,
        limit: limit
      },
      headers: {
        token: token
      },
      success: function (res) {
        console.log(res);
        let str = res.data.list.data.map((item, index) => {
          return `
          <article class="excerpt excerpt-one">
                <header>
                  <a class="cat label label-important" href="javascript:;">${item.catename}}<i class="label-arrow"></i></a>
                  <h2>
                    <a target="_blank" href="show.html" title="${item.title}}">${item.description}}</a>
                  </h2>
                </header>
                <p class="text-muted time"><a href="javascript:;">${item.author}}</a> 发布于 ${new Date(+item.create_date).toLocaleDateString()}}</p>
                <p class="focus">
                  <a target="_blank" href="javascript:;" class="thumbnail">
                    <span class="item"
                      ><span class="thumb-span"><img src="${QTURL + item.pic}" class="thumb" /></span></span
                  ></a>
                </p>
                <p class="note">${item.description}}</p>
                <p class="text-muted views">
                  <span class="post-views">阅读(${item.click}}})</span>
                  <span class="post-comments">评论(0)</span>
                  <a href="javascript:;" class="post-like"> <i class="fa fa-thumbs-up"></i>赞 (<span>${item.lovenum}}</span>) </a>
                  <span class="post-tags">标签： <a href="javascript:;" rel="tag">${item.tags ? item.tags : ''}}</a> / <a href="" rel="tag">${item.keywords}}</a></span>
                  <button class="btn btn-xs btn-danger" data-id=${item.id}><i class="fa fa-trash"></i>&nbsp;取消收藏</button>
                </p>
              </article>
          `
        }).join('')
        // 渲染
        $('.list-box').html(str)
        renderPage(res.data.list.count)
      }

    })
  }

  render()



  // 分页
  function renderPage(total) {
    // console.log(total);
    laypage.render({
      elem: 'test1',
      count: total,
      limit: q.pagesize,
      curr: q.pagenum,
      layout: ['count', 'limit', 'prev', 'page', 'next', 'skip'],
      limits: [1, 2, 3, 4, 5],
      jump: function (obj, first) {
        // console.log(obj.curr);
        q.pagenum = obj.curr
        q.pagesize = obj.limit


        //首次不执行
        if (!first) {
          //do something
          render(type = '2', q.pagenum, q.pagesize)

        }
      }
    })
  }


  // 取消
  $('.list-box').on('click', '.btn', function () {

    let article_id = $(this).attr('data-id')
    // console.log(article_id);
    // console.log(token);
    $.ajax({
      method: 'GET',
      url: '/user/userDataHandle',
      data: {
        type: '1',
        article_id: article_id,
        action: 'del'
      },
      headers: {
        token: token
      },
      success: function (res) {
        console.log(res);
        render()
      }
    })
  })





})