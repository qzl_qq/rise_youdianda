
$(function () {

  let imgURL = ''

  // 判断是否是从我的文章页面跳转
  function getId() {
    // 获取网址
    let str = location.search
    // 判断是否是跳转而来
    if (str.indexOf('?') == -1) return
    if (str) {
      let url = str.substr(1)
      let arr = url.split('&')
      let len = arr.length
      let request = {}

      for (let i = 0; i < len; i++) {
        request[arr[i].split('=')[0]] = arr[i].split('=')[1]
      }
      return request
    }

  }


  // 渲染下拉框  复选框
  function render() {
    $.ajax({
      method: 'GET',
      url: '/index/index',
      success: function (res) {
        // console.log(res);
        // 下拉框
        let Cate = res.data.allCate
        // 复选框
        let Tag = res.data.allTag
        // 渲染下拉框
        let CateStr = Cate
          .map((item) => {
            return `<option value="${item.id}">${item.catename}</option>`
          })
          .join('')
        // 不能选用html，因为html是直接添加，会直接显示选项的第一项   append是添加到里面
        $('#allCate').append(CateStr)
        //  渲染复选框
        let TagStr = Tag
          .map((item) => {
            return `<label><input type="checkbox" data-tag=${item.tagname} />${item.tagname}</label>`
          })
          .join('')
        $('#allTag').html(TagStr)
      },
    })
  }
  render()


  // 图片上传
  $('#addImg').click(function () {
    $('#file').click()
  })
  // 隐藏框   change事件
  $('#file').on('change', function (e) {
    let files = e.target.files
    // 通过   length  判断用户是否上传
    if (files.length === 0) return
    // 上传完毕后给该图片一个链接地址
    let imgURL = URL.createObjectURL(files[0])
    // 给图片框的  src 设置属性
    $('#preview').attr('src', imgURL)
    getImg()
  })

  function getImg() {
    let formData = new FormData()
    let file = $('#file')[0].files
    // console.log(file)
    formData.append('file', file[0])
    // console.log(formData)
    $.ajax({
      method: 'post',
      url: '/common/upload?type=images',
      data: formData,
      // 告诉jQuery不要去设置Content-Type请求头
      contentType: false,
      // 告诉jQuery不要去处理发送的数据
      processData: false,
      success: function (res) {
        imgURL = res.data.savePath
      },
    })
  }
  // 获取表单数据
  function getAddData(imgURL, status) {
    let title = $('#title').val()
    let author = ''
    let cateid = parseInt($('#allCate').val())
    // 可能选中多个标签
    let tags = $('#allTag input:checked')
    // console.log(tags);
    let tagList = []
    $.each(tags, function (i, item) {
      tagList.push(item.dataset.tag)
    })
    let tag = tagList.join(',')
    let content = $('.context').val()
    // 非空判断
    if (!title || !cateid || !tag || !content) {
      alert('请完成内容')
      return
    }
    let data = {
      title: title,
      author: author,
      cateid: cateid,
      tags: tag,
      pic: imgURL,
      content: content,
      status: status,

    }
    return data
  }
  $('.btn-box').on('click', 'button', function (e) {
    let status
    // 取消
    if (e.target.classList.contains('cancel')) {
      $('#title').val('')
      $('#allCate option:selected').prop('selected', false)
      $('#allTag input').prop('checked', false)
      $('.context').val('')
      $('#preview').attr('src', './static/images/default.png')
      return
    } else if (e.target.classList.contains('add')) {
      // 发布
      status = 1
    } else if (e.target.classList.contains('edit')) {
      // 草稿
      status = 2
    }
    // 调用表单内容函数
    let data = getAddData(imgURL, status)
    let id = getId()
    // 判断是否从文章页面跳转，根据网址是否有  id 
    if (id) {
      // 此时的id是一个对象，要转化为字符串
      let Id = Object.values(id).join('')
      // console.log(Id);
      data.id = Id
    }
    postData(id, data)
  })
  // 发布
  function postData(id, data) {
    // 根据网址是否给  id 来判断是发布还是修改
    let url = id ? '/user/editArticle' : '/user/addArticle'
    $.ajax({
      method: 'POST',
      url: url,
      data: data,
      success: function () {

        // alert('提交成功！')
        // 清空 模拟取消事件
        $('.btn-box .cancel').click()
      },
    })
  }
})