$(function () {
	var laypage = layui.laypage;
	let q = {
		pagenum: 1,
		pagesize: 1,
		cate_id: '',
		state: ''
	};
	//  type=2代表收藏
	let type = 2
	// 代表取消
	let action = 'del'
	let token = localStorage.getItem('token')

	function render(type = '2', page, limit) {
		// 请求
		$.ajax({
			method: 'GET',
			url: '/user/userDataList',
			data: {
				type: type,
				page: page,
				limit: limit,
			},
			headers: {
				token: token,
			},
			success: function (res) {
				console.log(res);
				let str = res.data.list.data
					.map(function (item, index) {
						return `
           <li>
                    <a target="_blank" href="javascript:;">
                      <span class="thumbnail">
                        <img class="thumb" src="${QTURL + item.pic}" />
                      </span>
                      <span class="text">${item.description}}</span>
                      <span class="text-muted post-views">已点赞(${item.lovenum})</span>
                      <button class="btn btn-xs btn-danger" data-id="${item.id}"><i class="fa fa-trash"></i>&nbsp;取消点赞</button>
                    </a>
                  </li>
          `
					})
					.join('')
				// 渲染
				$('.list-box').html(str)
				renderPage(res.data.list.count)
			},
		})
	}

	render()


	// 分页
	function renderPage(total) {
		// console.log(total);
		laypage.render({
			elem: 'test1',
			count: total,
			limit: q.pagesize,
			curr: q.pagenum,
			layout: ['count', 'limit', 'prev', 'page', 'next', 'skip'],
			limits: [1, 2, 3, 4, 5],
			jump: function (obj, first) {
				// console.log(obj.curr);
				q.pagenum = obj.curr
				q.pagesize = obj.limit


				//首次不执行
				if (!first) {
					//do something
					render(type = '2', q.pagenum, q.pagesize)

				}
			}
		})
	}

	// 取消点赞
	$('.list-box').on('click', 'button', function (e) {
		// 禁止跳转
		e.preventDefault()
		// 获取点击按钮的id
		let article_id = e.target.dataset.id

		$.ajax({
			method: 'GET',
			url: '/user/userDataHandle',
			data: {
				type: type,
				article_id: article_id,
				action: action,
			},
			headers: {
				token: token,
			},
			success: function (res) {
				// console.log(res);
				// 重新渲染
				render()
			},
		})
	})
})
