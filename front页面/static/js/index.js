$(function () {
	let token = localStorage.getItem('token')
	let layer = layui.layer

	axios.get(QTURL).then((res) => {
		let res1 = res.data.data
		// console.log(res1)
		// let nav_li = ''
		// 头部nav模块开始
		let nav_li = res1.allCate.map((item) => {
			return `
			          <li class="menu-item menu-item-type-taxonomy menu-item-object-category" data-id="${item.id}"><a href="javascript:;"><span
                class="fa ${item.icon}"></span>${item.catename}</a></li>
			`
		})
		$('.nav').append(nav_li)
		// 头部nav模块结束

		// 大轮播图模块开始
		let banner_str = res1.banner.map((item) => {
			return `
			<a target="_blank" href="${item.advimglink}" class="focusmo">
  			<img src="${QTURL + item.advimgsrc}">
  			<h4>${item.advimgdesc}</h4>
			</a>
			`
		})
		$('#banner_str').html(banner_str)
		// 轮播图实现代码
		layui.use('carousel', function () {
			var carousel = layui.carousel
			//建造实例
			carousel.render({
				elem: '#test1',
				width: '100%', //设置容器宽度
				arrow: 'always', //始终显示箭头
				// anim: 'updown', //切换动画方式
				interval: 2000, //自动播放时间
				anim: 'fade', //切换形态 （淡入淡出）
			})
		})
		// 大轮播图模块结束

		//热门推荐模块开始
		let sticky_str = res1.sideBanner.map((item) => {
			return `
			<li class="item">
				<a target="_blank" href="${item.advimglink}">
				<img src="${QTURL + item.advimgsrc}" class="thumb" />
				${item.advimgdesc}</a>
			</li>
			`
		})
		$('#tuijian').html(sticky_str)
		//热门推荐模块结束

		//右侧banner广告位开始
		let advertising_str = res1.sideBanner.map((item) => {
			return `
			<li class="item">
        <a href="${item.advimgdesc}" rel="nofollow" target="_blank" title="${item.advimglink}">
        <img class="g430" style="width:360px;height:133px;" src="${QTURL + item.advimgsrc}" alt="${
				item.advimglink
			}">
        </a>
			</li>
			`
		})
		$('#advertising').html(advertising_str)
		//右侧banner广告位结束

		//通知公告开始
		$('.style02_p').html(res1.config.allData[15].attr_value)
		//通知公告结束

		// 热门标签开始
		let widget_tags_innerStr = res1.allTag.map(
			(item) => `<li data-id="${item.id}"><a title="" href="">${item.tagname}</a></li>`
		)
		$('.widget_tags_inner').html(widget_tags_innerStr)
		// 热门标签结束

		// 友情链接开始
		let friendlink_str = res1.allLink.map((item) => {
			return `
			<li data-id="${item.id}">
        <a href="${item.url}"><img src="${QTURL + item.img}" alt=""><p>${item.des}</p></a>
      </li>
			`
		})
		$('#friendlink').html(friendlink_str)
		// 友情链接结束
	})

	//最新更新开始

	let laypage = layui.laypage

	let page = 1
	function render(page) {
		$.ajax({
			url: '/index/new/',
			method: 'GET',
			data: { page: page, limit: 10 },
			success: function (res) {
				// 首页文章

				console.log(res.data.list)
				// let res1 = res.data.list.data
				let update_str = res.data.list.data.map(function (item) {
					return `
			  	<article class="excerpt excerpt-one">
            <header">
              <a class="cat label label-important" href="">${
								item.ishot ? '热门' : '置顶'
							}<i class="label-arrow"></i></a>
              <h2>
                <a data-id="${
									item.id
								}" target="_blank" href="show.html" title="${item.title}" >${item.title}</a>
              </h2>
            </header>
            <p class="text-muted time">
              <a href="">${item.author}</a> 发布于 ${new Date(+item.create_date).toLocaleString()}
            </p>
            <p class="focus">
              <a href="http://blog.weknow.cn/474.shtml" class="thumbnail">
                <span  class="item"><span class="thumb-span"><img  src="${
									QTURL + item.pic
								}"  class="thumb" /></span></span></a>
              </p>
            <p class="note">${item.description}</p>
            <p class="text-muted views">
              <span class="post-views">阅读(${item.click})</span>
              <span class="post-comments">评论(0)</span>
              <a href="javascript:;" class="post-like" data-id="${item.id}">
                <i class="fa fa-thumbs-up"></i>赞 (<span class="like_add">${item.lovenum}</span>)
              </a>
              <a href="javascript:;" class="post-save" data-id="${item.id}">
                <i class="fa fa-heart"></i>收藏
              </a>
              <span class="post-tags">标签：
                <a href=""  rel="tag">${item.keywords}</a> / 
                <a href=""  rel="tag">${item.catename}</a></span>
             </p>
         </article>`
				})
				$('.article').html(update_str)
				// console.log(11)
				$('article').on('click', 'h2 a', function (e) {
					// e.preventDefault()
					let id = $(this).data('id')
					// console.log(id)
					localStorage.setItem('list-con_id', id)
				})

				// 收藏
				// 此类名不可以与index.html页面相同会造成连续点击
				$('.post-save').on('click', function (e) {
					// console.log(11)
					let article_id = $(this).data('id') + ''
					console.log(article_id)
					axios
						.get(QTURL + 'home/user/userDataHandle/', {
							params: { type: '1', article_id, action: 'add' },
							headers: { token: token },
						})
						.then((res) => {
							console.log(res)
							if (res.data.data === '操作成功') {
								layer.msg('收藏成功', { icon: 1 })
								$(this).css('color', 'orange')
							} else {
								layer.msg('您已经收藏过了', { icon: 2 })
								$(this).css('color', 'orange')
							}
						})
				})

				// 点赞
				$('.post-like').on('click', function () {
					// console.log(11)
					let article_id = $(this).data('id') + ''
					console.log(article_id)
					axios
						.get(QTURL + 'home/user/userDataHandle/', {
							params: { type: '2', article_id, action: 'add' },
							headers: { token: token },
						})
						.then((res) => {
							console.log(res)
							if (res.data.data === '操作成功') {
								layer.msg('点赞成功', { icon: 1 })
								$(this).css('color', 'red')
								document.querySelector('.like_add').innerHTML++
							} else {
								layer.msg('您已经点赞过了', { icon: 2 })
								$(this).css('color', 'red')
							}
						})
				})
				// console.log(res.data.list.count)
				fy(res.data.list.count)
			},
		})
	}

	render(page)
	// $('#test99').on('click', function (e) {
	// 	console.log(11)
	// 	return (page = 2) && render(page)
	// })

	// layui分页
	function fy(total) {
		layui.use('laypage', function () {
			var laypage = layui.laypage
			laypage.render({
				elem: 'test99', //注意，这里的 test1 是 ID，不用加 # 号
				count: total, //数据总数，从服务端得到
				limit: 10,
				curr: page,

				jump: function (obj, first) {
					//obj包含了当前分页的所有参数，比如：
					// console.log(obj.curr) //得到当前页，以便向服务端请求对应页的数据。
					// console.log(obj.limit) //得到每页显示的条数
					page = obj.curr
					//首次不执行
					if (!first) {
						//do something
						// console.log(11)
						render(page)
					}
				},
			})
		})
	}

	//热门文章开始

	axios.get(QTURL + 'index/hot/').then((res) => {
		let res1 = res.data.data.list
		// console.log(res1)

		let article_str = res1.map((item) => {
			return `
			 <li data-id="${item.id}">
					<a target="_blank" href="show.html">
						<span class="thumbnail">
							<img src="${QTURL + item.pic}" class="thumb" />
						</span>
						<span class="text">${item.title}</span>
						<span class="text-muted post-views">阅读(${item.click})</span>
					</a>
      	</li>
			`
		})
		$('.items-02').html(article_str)
		$('.items-02').on('click', 'li', function () {
			let id = $(this).data('id')
			localStorage.setItem('list-con_id', id)
		})
	})

	// 最新推荐开始

	axios.get(QTURL + 'index/recommend/').then((res) => {
		let res1 = res.data.data.list
		// console.log(res1)
		let tuijian1 = res1.map((item) => {
			return `
			      <li data-id="${item.id}">
              <a target="_blank" href="./show.html">
                <span class="thumbnail">
                    <img class="thumb"  src="${QTURL + item.pic}" />
                </span>
                <span class="text">${item.title}</span>
                <span  class="text-muted post-views">阅读(${item.click})</span>
              </a>
            </li>
			`
		})
		$('.items-01').html(tuijian1)
		$('.items-01').on('click', 'li', function () {
			let id = $(this).data('id')
			localStorage.setItem('list-con_id', id)
		})
	})

	// 头部nav点击跳转
	$('.nav').on('click', 'li', function (e) {
		// console.log($(this).data('id'))
		location.href = './list.html'
		let cateid = $(this).data('id')
		localStorage.setItem('list_id', cateid)
		e.target.classList.add('active')

		// $(this).addClass('active')
	})
	// let id = $('.nav li').data('id')
	// console.log(id)
})
