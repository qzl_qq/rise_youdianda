$(function () {
	let layer = layui.layer
	let cateid = localStorage.getItem('list_id')
	let token = localStorage.getItem('token')
	let article_id = ''

	// 头部nav点击跳转
	$('.nav').on('mouseover', 'li', function (e) {
		// console.log($(this).data('id'))
		// location.href = './list.html'
		let cateid1 = $(this).data('id')
		localStorage.getItem('list_id')
		// e.target.classList.add('active')

		if (cateid == cateid1) {
			// console.log(11)
			e.target.classList.add('active')
			// $(this).classList.addClass('active').siblings().removeClass('active')
		}

		// $(this).addClass('active')
	})
	// let id = $('.nav li').data('id')
	// console.log(id)
	let page = 1
	let laypage = layui.laypage

	function render(page) {
		axios.get(QTURL + 'index/list', { params: { page: page, limit: '10', cateid } }).then((res) => {
			let res1 = res.data.data.list.data
			// console.log(res.data.data.list)
			let qushi_str = res1.map((item) => {
				$('.cat-leader').html(`<h1>${item.catename}</h1><div class="cat-leader-desc"></div>`)
				return `
			  <article class="excerpt excerpt-one" data-id="${item.id}">
            <header>
              <a class="cat label label-important" href="">${
								item.ishot ? '热门' : '置顶'
							}<i class="label-arrow"></i></a>
              <h2>
                <a target="_blank" href="show.html"  title="${item.title}">${item.title}</a>
              </h2>
            </header>
            <p class="text-muted time">
              <a href="">${item.author}</a> 发布于 ${new Date(+item.create_date).toLocaleString()}
            </p>
            <p class="focus">
              <a target="_blank" href="http://blog.weknow.cn/474.shtml" class="thumbnail">
                <span  class="item"><span class="thumb-span"><img  src="${
									QTURL + item.pic
								}"  class="thumb" /></span></span></a>
              </p>
            <p class="note">${item.description}</p>
            <p class="text-muted views">
              <span class="post-views">阅读(${item.click})</span>
              <span class="post-comments">评论(0)</span>
              <a href="javascript:;" class="post-like1" data-id="${item.id}">
                <i class="fa fa-thumbs-up"></i>赞 (<span class="like_add">${
									item.lovenum * 1
								}</span>)
              </a>
							<a href="javascript:;" class="post-save1" data-id="${item.id}">
                <i class="fa fa-heart"></i>收藏
              </a>
              <span class="post-tags">标签：
                <a href=""  rel="tag">${item.keywords}</a> / 
                <a href=""  rel="tag">${item.catename}</a></span>
             </p>
         </article>
			`
			})
			// $('.cat-leader').after(qushi_str)
			$('#article').html(qushi_str)

			// 收藏
			// 此类名不可以与index.html页面相同会造成连续点击
			$('.post-save1').on('click', function (e) {
				// console.log(11)
				article_id = $(this).data('id') + ''
				console.log(article_id)
				axios
					.get(QTURL + 'home/user/userDataHandle/', {
						params: { type: '1', article_id, action: 'add' },
						headers: { token: token },
					})
					.then((res) => {
						console.log(res)
						if (res.data.data === '操作成功') {
							layer.msg('收藏成功', { icon: 1 })
							$(this).css('color', 'orange')
						} else {
							layer.msg('您已经收藏过了', { icon: 2 })
							$(this).css('color', 'orange')
						}
					})
			})

			// 点赞
			$('.post-like1').on('click', function () {
				// console.log(11)
				let article_id = $(this).data('id') + ''
				console.log(article_id)
				axios
					.get(QTURL + 'home/user/userDataHandle/', {
						params: { type: '2', article_id, action: 'add' },
						headers: { token: token },
					})
					.then((res) => {
						console.log(res)
						if (res.data.data === '操作成功') {
							// render(page)
							layer.msg('点赞成功', { icon: 1 })
							$(this).css('color', 'red')
							document.querySelector('.like_add').innerHTML++

							// console.log($('.like_add' + res1.id).text())
						} else {
							layer.msg('您已经点赞过了', { icon: 2 })
							$(this).css('color', 'red')
						}
					})
			})
			// console.log(res.data.data.list.count)
			fy(res.data.data.list.count)
		})
	}

	render(page)
	// $('#test99').on('click', function (e) {
	// 	console.log(11)
	// 	return (page = 2) && render(page)
	// })

	// layui分页
	function fy(total) {
		layui.use('laypage', function () {
			var laypage = layui.laypage
			laypage.render({
				elem: 'test98', //注意，这里的 test1 是 ID，不用加 # 号
				count: total, //数据总数，从服务端得到
				limit: 10,
				curr: page,

				jump: function (obj, first) {
					//obj包含了当前分页的所有参数，比如：
					// console.log(obj.curr) //得到当前页，以便向服务端请求对应页的数据。
					// console.log(obj.limit) //得到每页显示的条数
					page = obj.curr
					//首次不执行
					if (!first) {
						//do something
						// console.log(11)
						render(page)
					}
				},
			})
		})
	}

	//点击单个列表详情
	$('#qushi').on('click', 'article', function () {
		const id = $(this).data('id')
		localStorage.setItem('list-con_id', id)
		// console.log(id)
	})
})
