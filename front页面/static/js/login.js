$(function () {
	let layer = layui.layer

	$('.btn').on('click', function (e) {
		e.preventDefault()
		const phoneNumber = +$('#phone').val().trim()
		const pwd = $('#pwd').val().trim()
		if (!phoneNumber || !pwd) {
			return layer.msg('请输入账号和密码', { icon: 5 })
		}
		// console.log(typeof phoneNumber, typeof pwd)
		$.ajax({
			type: 'POST',
			url: '/index/login',
			data: { mobile: phoneNumber, password: pwd },
			success: function (res) {
				// console.log(res)
				if (res.errno !== 0) {
					return layer.msg(res.errmsg, { icon: 5 })
				}
				localStorage.setItem('token', res.data.token)
				location.href = './index-主页.html'
			},
		})
	})
})
