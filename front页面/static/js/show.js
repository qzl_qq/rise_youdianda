// import '../../../jquery.js'
$(function () {
	let token = localStorage.getItem('token')
	let layer = layui.layer

	// console.log(id)

	function render() {
		let id = localStorage.getItem('list-con_id')

		// 第一次请求获得show页面需要的数据
		axios.get(QTURL + 'home/index/show/', { params: { id: id } }).then((res) => {
			// 二次请求获得广告图
			let advertising1 = ''
			let advertising2 = ''
			axios.get(QTURL).then((RES) => {
				// console.log(RES.data.data.showBanner[0])
				advertising1 = RES.data.data.showBanner[0].advimgsrc
				advertising2 = RES.data.data.showBanner[1].advimgsrc
				// console.log(advertising1)
				// console.log(advertising2)
				// 渲染主列表
				// console.log(res.data.data)
				let res1 = res.data.data.info //主体内容
				let res2 = res.data.data.next[0] //下一个
				let res3 = res.data.data.prev[0] //上一个
				let res4 = res.data.data.recommend //推荐

				// 左侧header 渲染
				let con_str = `
    <header class="article-header">
    
        <!-- 面包屑 -->
        <div class="breadcrumbs">
          <span class="text-muted">当前位置：</span>
          <a href="./index-主页.html">优点达资讯</a>
          <small>></small> 
          <a href="./list.html">${res1.catename}</a> <small>></small> 
          <span class="text-muted">正文</span>
        </div>
        <!-- 标题 -->
        <h1 class="article-title"><a href="">${res1.title}</a></h1>
        <!-- 标题描述 -->
        <ul class="article-meta">
              <li><a href="">${res1.author}</a> 发布于 ${new Date(
					+res1.create_date
				).toLocaleString()}</li>
          <li>分类：<a href="" rel="category tag">${res1.catename}</a>li>
          <li><span class="post-views">阅读(${res1.click})</span></li>
          <li>评论(1)</li>
          <li></li>
        </ul>
    </header>
            
        <!-- 广告位 -->
        <div class="ssr ssr-content ssr-post">
            <center>
              <a href="" title="" target="_blank" rel="nofollow">
                <img src="${
									QTURL + advertising2
								}" title="领取优惠券" style="width: 728px;height:90px;" alt="">
              </a>
            </center>
        </div>

        <!-- 文章正文 -->
          <article class="article-content">
            <!-- 文章内容开始 -->
              <p>${res1.content}</p>
              <!-- 文章内容结束 -->
              
              <!-- 版权描述 -->
              <p class="post-copyright">未经允许不得转载：
              <a href="./index-主页.html">优点达资讯</a> &raquo; <a href="">${res1.title}</a></p>
          </article>

        <!-- 点赞 -->
          <div class="article-social">
            <a href="javascript:;" class="action action-like" data-id=${res1.id}>
              <i class="glyphicon glyphicon-thumbs-up"></i>赞 (<span class="like_ok">${
								res1.lovenum
							}</span>)
            </a> 
          </div>
          <!-- 标签信息 -->
          <div class="article-tags">
            标签：<a href="" rel="tag">${res1.keywords}</a> 
          </div>

        <!-- 上下一篇 -->
          <nav class="article-nav">
            <span class="article-nav-prev">
              <span>上一篇</span>
              <a href="" rel="prev" data-id="${res2.id}">${res2.title}</a>
            </span>
            <span class="article-nav-next">
              <span>下一篇</span>
              <a href="" rel="prev" data-id="${res3.id}">${res3.title}</a>
            </span>
          </nav>

        <!-- 广告位 -->
          <div class="ssr ssr-content ssr-related">
            <center>
              <a href="" rel="nofollow" title="" target="_blank">
                  <img src="${
										QTURL + advertising2
									}" title="领取优惠券"style="width: 728px;height:90px;" alt="">
              </a>
            </center>
          </div>

        <!-- 推荐 -->
          <div class="relates relates-model-thumb">
            <h3 class="title"><strong>相关推荐</strong></h3>
            <ul>
              
            </ul>
          </div>

		  `
				$('#con').html(con_str)

				let map_str = res4.map((item) => {
					return `
        <li>
          <a target="_blank" href="" data-id="${item.id}">
            <span>
              <img src="${QTURL + item.pic}" class="thumb" />
            </span>
            ${item.title}
          </a>
        </li>
        `
				})
				$('.relates ul').html(map_str)
				$('.action-like').on('click', function () {
					// console.log(11)
					let article_id = $(this).data('id') + ''
					console.log(article_id)
					axios
						.get(QTURL + 'home/user/userDataHandle/', {
							params: { type: '2', article_id, action: 'add' },
							headers: { token: token },
						})
						.then((res) => {
							// console.log(res)
							if (res.data.data === '操作成功') {
								layer.msg('点赞成功', { icon: 1 })
								document.querySelector('.like_ok').innerHTML++
								$(this).css('color', 'red')
							} else {
								layer.msg('您已经点赞过了', { icon: 2 })
							}
						})
				})
			})
		})
	}
	render()

	// 触发点击事件重新渲染
	$('#con').on('click', 'a', function (e) {
		// e.preventDefault()
		// console.log($(this).data('id'))
		let id = $(this).data('id')

		if (id) {
			console.log($(this).data('id'))
			localStorage.setItem('list-con_id', id)
			render()
		}
	})
})
