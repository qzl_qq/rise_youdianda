$(function () {
  // 定义token
  let token = localStorage.getItem('x-token');
  // console.log(token);
  let q = {
    start_time: '',
    end_time: '',
    catename: '',
    title: '',
    cateid: '',
    page: '1',
    limit: '10',
  };
  // 请求数据
  function getData() {
    axios({
      method: 'get',
      url: UART + '/ydd_cate/list',
      params: q,
      headers: {
        'x-token': token,
      },
    }).then(function (res) {
      // console.log(res);
      let data = res.data.data;
      // 分页和总页数
      let count = res.data.data.count;
      // console.log(count);
      let curr = res.data.data.currentPage;
      // 函数调用
      List(data);
      fw(count, curr);
    });
  }
  getData();

  //渲染列表
  function List(data) {
    let reg = '';
    data.data.forEach((item, index) => {
      reg += `<tr>
    <td style="text-align: center;">${index + 1}</td>
    <td style="text-align: center;" class="q_id">${item.id}</td>
    <td style="text-align: center;" class="q_catename">${item.catename}</td>
    <td style="text-align: center;" class="q_icon">${item.icon}<i class="fa ${item.icon}"></i></td>
    <td style="text-align: center;" class="q_sort_num">${item.sort_num}</td>
    <td style="text-align: center;"> 
    <button type="button" style="width:40px;height:40px;border-radius:50% ;background:#27a3ff; color:#fff; border:#27a3ff" class="classRevise" value="${item.id} ">
    <i class="layui-icon layui-icon-edit" ></i>
    </button>
    <button type="button" value="${item.id}" class="classDel" style="width:40px;height:40px;border-radius:50% ;background:#fc522a; color:#fff; border:#fc522a; margin-left: 10px;" >
    <i class="layui-icon layui-icon-delete" ></i>  
    </button>
  </td>
  </tr>
    `;
    });

    $('tbody').html(reg);
  }
  // 底部分页
  let laypage = layui.laypage;
  function fw(count, curr) {
    laypage.render({
      elem: 'pageBox', //分页容器
      count: count, //数据总数
      limit: q.limit, //每页总数
      curr: curr, //页数 // // 定义不同功能
      theme: '#27a3ff',
      layout: ['count' /*条数*/, 'limit' /*下拉选页数*/, 'prev' /*上一页*/, 'page' /*每一页*/, 'next' /*下一页*/, 'skip' /*切换分页*/],
      limits: [2, 4, 6, 8, 10], // 每次切换不同的页数都会重新执行渲染函数
      jump: function (obj, first) {
        //obj包含了当前分页的所有参数，比如：
        // console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
        // console.log(obj.limit); //得到每页显示的条数
        q.page = obj.curr;
        q.limit = obj.limit;
        //首次不执行
        if (!first) {
          //do something
          getData();
        }
      },
    });
  }
  fw();

  // 点击隐藏搜索
  let one = true;
  $('#vanishTop').on('click', function () {
    // console.log(1);
    if (one) {
      // 搜索区隐藏
      $('.management').hide();
      // 按钮旋转
      $(this).css('transform', 'rotate(180deg)');
      // 切换one状态
      one = false;
      return;
    }
    $('.management').show();
    $(this).css('transform', 'rotate(360deg)');
    one = true;
  });
  // 重置按钮
  $('.one').on('click', function () {
    $('.Q_search').val('');
  });

  // 小下拉框
  let two = true;
  $('#Q_one').click(function () {
    // console.log(1);
    if (two) {
      //
      $('.adv-box').show();
      two = false;
    } else {
      $('.adv-box').hide();
      two = true;
    }
  });

  // 默认全部选中
  $('.check_img').prop('checked', true);
  let btt = document.querySelectorAll('.check_img');
  for (let i = 0; i < btt.length; i++) {
    // 设置属性名和属性值
    btt[i].setAttribute('index', i);
    btt[i].onclick = function () {
      index = this.getAttribute('index');
      if (index == 0) {
        $('.q_id').toggleClass('all');
      } else if (index == 1) {
        $('.q_catename').toggleClass('all');
      } else if (index == 2) {
        $('.q_icon').toggleClass('all');
      } else if (index == 3) {
        // console.log(1);
        $('.q_sort_num').toggleClass('all');
      }
    };
  }
  // 搜索区域
  $('.q_search').click(function () {
    // 根据搜索的数据更换
    q.catename = $('[name=title]').val();
    // 重新提交渲染
    getData();
  });
  // 重置按钮
  $('#Refresh').click(function () {
    console.log(1);
    // 搜索框内容清空
    $('.Q_search').val('');
    getData();
  });

  // 添加
  $(function () {
    let layer = layui.layer;
    let form = layui.form;
    let indexAdd = null;
    // 添加
    $('#xinzeng').on('click', function () {
      indexAdd = layer.open({
        type: 1,
        area: ['650px', '300px'],
        title: ['添加分类', 'font-size:18px;'],
        content: $('#dialog-add').html(), //这里content是一个普通的String
      });
      // 事件委托进行提交
      $('body').on('submit', '#classAdd', function (e) {
        e.preventDefault();
        axios({
          method: 'post',
          url: UART + '/ydd_cate/add',
          data: $(this).serialize(),
          headers: {
            'x-token': token,
          },
        }).then(function (res) {
          if (res.status !== 200) {
            return layer.msg('添加失败');
          }
          getData();
          return layer.msg('添加成功');
          // 根据索引值关闭弹出层
        });
        layer.close(indexAdd);
      });
      $('body').on('click', '.Q_Cancel', function () {
        layer.close(indexAdd);
      });
    });

    // 修改
    let indexEdit = null;
    $('#tbody').on('click', '.classRevise', function () {
      indexEdit = layer.open({
        type: 1,
        area: ['650px', '300px'],
        title: ['修改分类', 'font-size:18px;'],
        content: $('#dialog-Revise').html(), //这里content是一个普通的String
      });
      // classRevise;
      let id = $(this).prop('value');
      console.log(id);
      // 发起请求获取对应分类的数据
      $.ajax({
        method: 'get',
        url: UART + `/ydd_cate/rowInfo`,
        data: {
          id: id,
        },
        headers: {
          'x-token': token,
        },
        success: function (res) {
          form.val('classRevise', res.data);
        },
      });
    });
    // 通过事件委托，为修改表单绑定submit事件
    $('body').on('submit', '#classRevise', function (e) {
      e.preventDefault();
      axios({
        method: 'post',
        url: UART + '/ydd_cate/update',
        data: $(this).serialize(),
        headers: {
          'x-token': token,
        },
      }).then(function (res) {
        if (res.status !== 200) {
          return layer.msg('修改失败');
        }
        getData();
        return layer.msg('修改成功');
        // 根据索引值关闭弹出层
      });
      layer.close(indexEdit);
    });
    // 取消按钮
    $('body').on('click', '.Q_Cancel2', function () {
      layer.close(indexEdit);
    });

    // 删除
    // let indexEdit = null;
    $('#tbody').on('click', '.classDel', function () {
      let id = $(this).prop('value');
      // console.log(id);
      layer.confirm('删除，is not?', { icon: 3, title: '提示' }, function (index) {
        // 删除链接
        axios({
          method: 'GET',
          url: UART + '/ydd_cate/del',
          params: { id: id },
          headers: { 'x-token': token },
        }).then((res) => {
          // 重新发起get数据请求 ，重新渲染
          getData();
          if (res.data.errno == 0) return layer.msg('删除成功');
          layer.msg(res.data.errmsg);
        }); // 关闭confirm弹窗
        layer.close(index);
      });
    });
  });
  // 导出文件
  $('#export').click(function (e) {
    let table = $('.layui-table');
    let preserveColors = table.hasClass('table2excel_with_colors') ? true : false;
    $(table).table2excel({
      exclude: '.noExl',
      name: 'Excel Document Name',
      filename: 'myFileName' + new Date().toISOString().replace(/[\-\:\.]/g, '') + '.xls',
      fileext: '.xls',
      exclude_img: true,
      exclude_links: true,
      exclude_inputs: true,
      preserveColors: preserveColors,
    });
  });
});
