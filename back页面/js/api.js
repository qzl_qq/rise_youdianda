axios.interceptors.request.use(function (config) {
    $(".tb").hide()
    $(".loading").css('display', 'block')
    // 在发送请求之前做些什么
    config.url = 'http://192.168.18.32:8060/admin' + config.url

    if (config.url.indexOf('admin' !== -1)) {
        config.headers['x-token'] = localStorage.getItem('x-token')
    }
    return config;
})
axios.interceptors.response.use(function (response) {
    $(".tb").show()
    $(".loading").css('display', 'none')
    return response;
})