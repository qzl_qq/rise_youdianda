$(function () {
  let url = 'http://192.168.18.32:8060/admin';
  let token = localStorage.getItem('x-token');
  // console.log(token);
  // 1、广告图列表分页
  let layer = layui.layer;
  let form = layui.form;
  let img_name = null;

  // 请求广告页数据要求
  let advPage = {
    page: '1', // 页码值，默认请求第一页的数据
    limit: '8', // 每页显示几条数据
    advimgpos: '', // 广告图所在广告位id
    advimgdesc: '', // 广告图描述
  };

  // 广告分页渲染
  img_render();
  function img_render() {
    axios({
      method: 'GET',
      url: url + '/ydd_advimg/list',
      params: advPage,
      headers: { 'x-token': token },
    }).then((res) => {
      // console.log(res.data);
      // 下拉框渲染
      img_name = res.data.data.data;
      pullList(img_name);
      // 分页部分
      let count = res.data.data.count;
      let curr = res.data.data.currentPage;
      page(count, curr);
      // map方法渲染数据
      let list = res.data.data.data;
      let lis = list.map((item) => {
        // console.log(item.advimgsrc);
        return `
          <tr>
              <td> <input class="check" type="checkbox"></td>
              <td>${item.id}</td>
              <td class="advposname_img">${item.advposname}</td>
              <td class="advimgdesc_img">${item.advimgdesc}</td>
              <td class="advimgsrc_img"> <img src="http://192.168.18.32:8060/${item.advimgsrc}" ></td>
              <td class="advimglink_img">${item.advimglink}</td>
              <th>
              <div class="layui-btn-group">
              <button type="button"  class="layui-btn layui-btn-sm  layui-btn-radius layui-btn-normal" data-id="${item.id}" id="amend";>
                <i class="layui-icon" >&#xe642;</i>
              </button>
              <button type="button" class="layui-btn layui-btn-sm layui-btn-radius layui-btn-danger btn-delete" data-id="${item.id}">
                <i class="layui-icon">&#xe640;</i>
              </button>
              </div></th>
          </tr>`;
      });
      // 添加到页面中
      $('tbody').html(lis);
    });
  }
  // 底部分页
  function page(count, curr) {
    let layPage = layui.laypage;
    //执行一个laypage实例
    layPage.render({
      elem: 'pageBox', //分页容器
      count: count, //数据总数
      limit: advPage.limit, //每页总数
      curr: advPage.page, //页数
      // // 定义不同功能
      layout: [
        'count' /*条数*/,
        'limit' /*下拉选页数*/,
        'prev' /*上一页*/,
        'page' /*每一页*/,
        'next' /*下一页*/,
        'skip' /*切换分页*/,
      ],
      limits: [2, 4, 6, 8, 10],
      // 每次切换不同的页数都会重新执行渲染函数
      jump: (obj, first) => {
        advPage.page = obj.curr;
        advPage.limit = obj.limit;
        if (!first) {
          img_render();
        }
      },
    });
  }

  // 全选框
  $('#img_Allbox').change(function () {
    // 全选框变化时 prop方法更改 单选框的checked状态，第二个参数是全选框是否选中
    $('.check').prop('checked', $(this).prop('checked'));
    // 删除按钮隐藏，消失
    if ($('#img_Allbox').prop('checked') !== true) {
      $('.shanchu_btn').hide();
    } else {
      $('.shanchu_btn').show();
    }
  });
  // 单选框 渲染数据需要用on委托事件
  // 选中的数量是否等于所有单选框的长度
  $('tbody').on('change', '.check', function () {
    if ($('.check:checked').length === $('.check').length) {
      $('#img_Allbox').prop('checked', true);
    } else {
      $('#img_Allbox').prop('checked', false);
    }
  });

  // 搜索区域
  $('#btn_ss').click(function () {
    // 根据搜索的数据更换
    advPage.advimgdesc = $('[name=advimgdesc]').val();
    advPage.advimgpos = $('[name=advimgpos]').val();
    // 重新提交渲染
    img_render();
  });
  // 重置按钮
  $('#btn_cz').click(function () {
    // 搜索框内容清空
    $('.sousuo').val();
    img_render();
  });

  // 广告图删除部分
  $('tbody').on('click', '.btn-delete', function () {
    // 对应id
    let id = $(this).attr('data-id');
    // 广告图数量
    let length = $('.btn-delete').length;
    // console.log(id,length);
    // layui 弹出确认框
    layer.confirm('?删除? yes or not', { icon: 3, title: '提示' }, function (index) {
      axios({
        method: 'GET',
        url: url + '/ydd_advimg/del',
        params: { id: id },
        headers: { 'x-token': token },
      }).then((res) => {
        // console.log(res);
        if (res.data.errno === 1000) {
          return layer.msg(res.data.errmsg);
        }
        layer.msg('删除成功！');
        // 若页面数据只有1 页面页面-1重新渲染出数据
        if (length === 1) {
          advPage.page = advPage.page = 1 ? 1 : advPage.page - 1;
        }
        // 重新渲染
        img_render();
      });
      // 结束后关闭页面
      layer.close(index);
    });
  });

  // 广告图修改部分
  let amend = null;
  let id_amend;
  $('tbody').on('click', '#amend', function () {
    id_amend = $(this).attr('data-id');
    // console.log(id_amend);
    amend = layer.open({
      type: 1,
      title: '修改广告位',
      content: $('#dialog-amend').html(),
      area: ['800px', '400px'],
    });
    $('#city').html(new_pullList);
    // 重新渲染数据
    form.render();

    // 获取对应的详细的数据
    axios({
      method: 'GET',
      url: url + '/ydd_advimg/rowInfo',
      params: { id: id_amend },
      headers: { 'x-token': token },
    }).then((res) => {
      // console.log(res.data);
      res_data = res.data.data.advimgpos;
      // lay-filter 通过layui里val（）方式渲染数据
      form.val('form-edit', res.data.data);
    });
  });

  // 下拉框渲染
  let new_pullList = null;
  function pullList(arr) {
    // console.log(arr);
    new_pullList = arr.map((item) => {
      return `<option value="${item.advimgpos}">${item.advposname}</option>`;
    });
    // console.log(new_pullList);
    // 首位添加默认提示选项
    new_pullList.unshift(' <option value="">请选择内容</option>');
    // 给搜索部分的下拉框也添加上下拉内容
    $('#city_one').html(new_pullList);
    form.render();
  }

  // 点击上传弹出文件
  $('body').on('click', '.shangchuan', function () {
    $('#file').click();
  });
  // 图片
  $('body').on('change', '#file', function (e) {
    $('#form2').submit();
  });
  // 提交获取图片路径
  let img = null;
  $('body').on('submit', '#form2', (e) => {
    // console.log(111);
    e.preventDefault();
    let fd = new FormData($('#form2')[0]);
    $.ajax({
      url: url + '/common/upload?type=images',
      method: 'POST',
      data: fd,
      headers: {
        'x-token': token,
      },
      // 注意：formData格式的请求要加上下面两项配置
      contentType: false,
      processData: false,
      success: function (res) {
        // console.log(res);
        img = res.data.savePath;
        $('#image')
          .show()
          .prop('src', 'http://192.168.18.32:8060/' + img);
      },
    });
  });
  // 修改渲染
  $('body').on('submit', '#form-amend', function (e) {
    e.preventDefault();
    let advimgdesc = $('#advimgdesc_amend').val();
    let advimglink = $('[name=advimglink]').val();
    let advimgpos = $('#city').val();
    // console.log(advimgdesc);
    axios({
      method: 'POST',
      url: url + '/ydd_advimg/update',
      data: {
        advimgpos: advimgpos,
        advimgdesc: advimgdesc,
        advimglink: advimglink,
        advimgsrc: img,
        id: id_amend,
      },
      headers: { 'x-token': token },
    }).then((res) => {
      // console.log(res);
      if (res.status !== 200) {
        return layer.msg('修改失败');
      }
      layer.msg('修改成功');
      img_render();
      layer.close(amend);
    });
  });
  // 取消修改按钮
  $('body').on('click', '.quxiao', function () {
    layer.close(amend);
  });

  // 添加
  $('.zengjia').on('click', function () {
    amend = layer.open({
      type: 1,
      title: '新增广告位',
      content: $('#dialog-add').html(),
      area: ['800px', '400px'],
    });
    // 下拉框
    $('#city').html(new_pullList);
    form.render();
  });

  // 添加渲染
  $('body').on('submit', '#form-add', function (e) {
    e.preventDefault();
    let obj = {};
    obj.advimgdesc = $('#advposdesc').val();
    obj.advimglink = $('#advimglink').val();
    obj.advimgpos = $('#city').val();
    obj.advimgsrc = img;
    // console.log(obj.advimgdesc);
    // console.log(obj.advimglink);
    axios({
      method: 'POST',
      url: url + '/ydd_advimg/add',
      data: obj,
      headers: {
        'x-token': token,
      },
    }).then((res) => {
      // console.log(res);
      if (res.status !== 200) {
        return layer.msg('添加失败');
      }
      layer.msg('添加成功');
      // 重新渲染
      img_render();
      // 关闭弹窗
      layer.close(amend);
    });
  });

  // 全选删除
  $('.shanchu_btn').on('click', function () {
    // 删除后默认标题
    $('table').html(
      '<tr><th>#</th><th class="advposname_yc">广告位名称</th><th class="advposdesc_yc">广告位描述</th><th class="default_data_yc">广告图数量</th><th class="advpossize_yc">广告位尺寸</th><th style="text-align: center">操作</th></tr>'
    );
    $('#foot').show();
    $('#pageBox').hide();
  });

  // 导出文件
  $('#export').click(function (e) {
    let table = $('.layui-table');
    let preserveColors = table.hasClass('table2excel_with_colors') ? true : false;
    $(table).table2excel({
      exclude: '.noExl',
      name: 'Excel Document Name',
      filename: 'myFileName' + new Date().toISOString().replace(/[\-\:\.]/g, '') + '.xls',
      fileext: '.xls',
      exclude_img: true,
      exclude_links: true,
      exclude_inputs: true,
      preserveColors: preserveColors,
    });
  });

  // 右上角的按钮
  let one = true; // 这是布尔值 用来判断状态
  $('.rigth_one').on('click', function () {
    if (one) {
      // 搜索区隐藏
      $('#adv_ss').hide();
      // 按钮旋转
      $(this).css('transform', 'rotate(180deg)');
      // 切换one 状态
      one = false;
      return;
    }
    $('#adv_ss').show();
    $(this).css('transform', 'rotate(360deg)');
    one = true;
  });
  // 重置按钮
  $('.layui-icon-refresh-3').on('click', function () {
    $('.sousuo').val('');
    img_render();
  });
  let two = true;
  $('.layui-icon-app').on('click', function () {
    if (two) {
      $('.adv-box').show();
      two = false;
    } else {
      $('.adv-box').hide();
      two = true;
    }
  });

  // 默认全部全部选中
  $('.check_img').prop('checked', true);
  // 给每一项添加点击事件
  let btns = document.querySelectorAll('.check_img');
  for (let i = 0; i < btns.length; i++) {
    // 设置属性名和属性值
    btns[i].setAttribute('index', i);
    btns[i].onclick = function () {
      index = this.getAttribute('index');
      // 判断点击对应项 然后设置隐藏不是删除
      if (index == 0) {
        $('.advposname_img').toggleClass('all');
      } else if (index == 1) {
        $('.advimgdesc_img').toggleClass('all');
      } else if (index == 2) {
        $('.advimgsrc_img').toggleClass('all');
      } else if (index == 3) {
        $('.advimglink_img').toggleClass('all');
      }
    };
  }
});
