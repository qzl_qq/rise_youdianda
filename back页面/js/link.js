$(function () {
  // 接口
  let url = 'http://192.168.18.32:8060/admin';
  let token = localStorage.getItem('x-token');
  let layer = layui.layer;
  let form = layui.form;
  let img = null;

  // 点击文章或者图片 对应部分淡入 另一个淡出
  $('#li1').click(() => {
    $('#two').fadeOut();
    setTimeout(function () {
      $('#one').fadeIn();
    }, 500);
  });
  $('#li2').click(() => {
    $('#one').fadeOut();
    setTimeout(function () {
      $('#two').fadeIn();
    }, 500);
  });

  // 查询提交数据对象
  let dataOne = {
    page: 1,
    type: 1,
    limit: 10,
  };
  let dataTwo = {
    page: 1,
    type: 2,
    limit: 10,
  };

  // 下拉框
  $(document).ready(function () {
    // select下拉框选中触发事件
    form.on('select(test1)', function (data) {
      let val = data.value;
      select = data.value;
      if (val == 0) {
        $('#title1').show();
        $('#title2').hide();
      } else {
        $('#title2').show();
        $('#title1').hide();
      }
    });
  });
  $(document).ready(function () {
    // select下拉框选中触发事件
    form.on('select(test2)', function (data) {
      let val = data.value;
      select = data.value;
      if (val == 0) {
        $('#title3').show();
        $('#title4').hide();
      } else {
        $('#title4').show();
        $('#title3').hide();
      }
    });
  });

  // 渲染文章部分
  renderText();
  function renderText() {
    axios({
      method: 'GET',
      url: url + '/ydd_link/list',
      params: dataOne,
      headers: { 'x-token': token },
    }).then((res) => {
      // console.log(res);
      if (res.status !== 200) return false;
      // 分页部分
      let count = res.data.data.count;
      let curr = res.data.data.currentPage;
      page(count, curr, dataOne);

      let list = res.data.data.data;
      let lis = list
        .map((item) => {
          // 解构
          const { id, title, des, url } = item;
          return `
          <tr>
              <td>${id}</td>
              <td>${title}</td>
              <td>${des}</td>
              <td><span class="text">文字类型</span></td>
              <td>${url}</td>
              <td class="btn">
                <button class="layui-btn layui-btn-normal" id="modify" value=${id}><i class="layui-icon layui-icon-edit"></i></button>
                <button class="layui-btn layui-btn-danger" id="remove" value=${id}><i class="layui-icon layui-icon-delete"></i></button>
              </td>
          </tr>`;
        })
        .join('');
      $('tbody').html(lis);
    });
  }

  // 渲染图片链接部分
  renderImg();
  function renderImg() {
    axios({
      method: 'GET',
      url: url + '/ydd_link/list',
      params: dataTwo,
      headers: { 'x-token': token },
    }).then((res) => {
      // console.log(res.data.data.data);
      if (res.status !== 200) {
        return false;
      }
      // 分頁
      let count = res.data.data.count;
      let curr = res.data.data.currentPage;
      page(count, curr, dataTwo);

      let list = res.data.data.data;
      let lis = list
        .map((item) => {
          const { id, img, des, url } = item;
          return `
          <tr>
              <td>${id}</td>
              <td>${des}</td>
              <td><img src="http://192.168.18.32:8060/${img}" alt=""></td>
              <td><span class="text">图片类型</span></td>
              <td>${url}</td>
              <td class="btn1">
                <button class="layui-btn layui-btn-normal" id="modify" value=${id}><i class="layui-icon layui-icon-edit"></i></button>
                <button class="layui-btn layui-btn-danger" id="remove" value=${id}><i class="layui-icon layui-icon-delete" ></i></button>
              </td>
          </tr>`;
        })
        .join('');
      $('.tbody1').html(lis);
    });
  }

  // 删除部分
  $('body').on('click', '#remove', function () {
    let id = $(this).prop('value');
    // console.log(id);
    layer.confirm('删除，is not?', { icon: 3, title: '提示' }, function (index) {
      // 删除链接
      axios({
        method: 'GET',
        url: url + '/ydd_link/del',
        params: { id: id },
        headers: { 'x-token': token },
      }).then((res) => {
        // console.log(res);
        // 重新渲染
        renderText();
        renderImg();
        if (res.data.errno == 0) return layer.msg('删除成功');
        layer.msg(res.data.errmsg);
      });
      // 关闭confirm弹窗
      layer.close(index);
    });
  });

  // 底部分页显示
  let name = null;
  function page(count, curr, data) {
    let laypage = layui.laypage;
    if (data === dataOne) {
      name = 'page';
    } else {
      name = 'page1';
    }
    //执行一个laypage实例
    laypage.render({
      elem: name, //分页容器
      count: count, //数据总数
      limit: data.limit, //每页总数
      curr: data.page, //页数
      // // 定义不同功能
      layout: [
        'count' /*条数*/,
        'limit' /*下拉选页数*/,
        'prev' /*上一页*/,
        'page' /*每一页*/,
        'next' /*下一页*/,
        'skip' /*切换分页*/,
      ],
      limits: [2, 4, 6, 8, 10],
      jump: (obj, first) => {
        data.page = obj.curr;
        data.limit = obj.limit;
        if (!first) {
          if (data === dataOne) {
            return renderText();
          }
          renderImg();
        }
      },
    });
  }

  // 提交圖片
  $('body').on('click', '#submit', function () {
    $('#files').click();
  });
  $('body').on('change', '#files', function (e) {
    $('#form2').submit();
  });
  $('body').on('submit', '#form2', (e) => {
    e.preventDefault();
    let fd = new FormData($('#form2')[0]);
    $.ajax({
      url: 'http://192.168.18.32:8060/common/upload?type=images',
      method: 'POST',
      data: fd,
      headers: {
        token: token,
      },
      // 注意：formData格式的请求要加上下面两项配置
      contentType: false,
      processData: false,
      success: function (res) {
        // console.log(res);
        img = res.data.savePath;
        // console.log(img);
        $('#image')
          .show()
          .prop('src', 'http://192.168.18.32:8060/' + img);
      },
    });
  });

  // 添加部分
  function add() {
    $('.add').click(() => {
      layer.open({
        title: '添加友情链接',
        area: ['683px', '400px'],
        content: `
        <form class="layui-form" action="" id="form1">
          <div class="layui-form-item">
            <label class="layui-form-label">链接类型:</label>
            <div class="layui-input-block">
              <select name="city" lay-verify="required" lay-filter="test1">
              <option value="">请选择类型</option>
                <option value="0">文字链接</option>
                <option value="1">图片链接</option>
              </select>
            </div>
          </div>
          <div class="layui-form-item" style="display: none" id="title1">
            <label class="layui-form-label">标题</label>
            <div class="layui-input-block">
              <input type="text" name="title" required lay-verify="required" placeholder="请输入标题名称:" autocomplete="off" class="layui-input" id='title5'/>
            </div>
          </div>
          <div class="layui-form-item">
            <label class="layui-form-label">链接类型:</label>
            <div class="layui-input-block">
              <input type="text" name="title" required lay-verify="required" placeholder="请输入链接地址名" autocomplete="off" class="layui-input" id="url"/>
            </div>
          </div>
          <div class="layui-form-item">
            <label class="layui-form-label">链接描述:</label>
            <div class="layui-input-block">
              <textarea name="desc" placeholder="请输入链接描述名称" class="layui-textarea"></textarea>
            </div>
          </div>
        </form>
        <form class="layui-form" action="" id="form2">
          <div class="layui-form-item" style="display: none" id="title2">
          <label class="layui-form-label">链接图片:</label>
          <div class="layui-input-block">
            <input type="file" name="file" id="files" style="display: none" accept="image/png,image/jpeg,image/gif"/>
            <button type="button" class="layui-btn" id="submit">点击上传</button>
            <img src="" id="image" style="width: 140px; height: 80px; display: none;"/>
          </div>
          </div>
        </form>`,
        btn: ['确定', '取消'],
        yes: function (index, layerO) {
          let type = '';
          let des = $('.layui-textarea').val().trim();
          let title = '';
          let url = $('#url').val().trim();
          if (select === '0') {
            type = 1;
            title = $('#title5').val().trim();
          } else if (select === '1') {
            type = 2;
          } else {
            return layer.msg('请选择链接类型');
          }
          axios({
            method: 'POST',
            url: 'http://192.168.18.32:8060/admin' + '/ydd_link/add',
            headers: {
              'x-token': token,
            },
            data: { des: des, img: img, title: title, type: type + '', url: url },
          }).then((res) => {
            console.log(res);
            if (res.data.errno !== 0) return layer.msg('添加失败');
            layer.msg('添加成功');
            if (type === 1) {
              renderText();
            } else {
              renderImg();
            }
          });
          layer.close(index);
        },
      });
      form.render();
    });
  }
  add();

  // 修改部分
  let amend = null;
  $('tbody').on('click', '#modify', function () {
    id = $(this).prop('value');
    // console.log(id);
    amend = layer.open({
      title: '添加友情链接',
      content: $('#dialog-amend').html(),
      area: ['800px', '400px'],
    });
    form.render();

    // 获取对应的详细数据
    axios({
      method: 'GET',
      url: url + '/ydd_link/rowInfo',
      params: { id: id },
      headers: { 'x-token': token },
    }).then((res) => {
      // console.log(res.data.data);
      // 具体类型是文字还是图片
      type = res.data.data.type;
      // console.log(type);
      // lay-filter 通过layui里val（）方式渲染数据
      form.val('form-edit', res.data.data);
    });
  });

  // 修改渲染
  $('body').on('submit', '#form-amend', function (e) {
    e.preventDefault();
    let des = $('.layui-textarea').val().trim();
    let linkUrl = $('#url').val().trim();
    let title = '';
    let type1 = type;
    // console.log(type1);
    if (type1 == 1) {
      title = $('#title').val().trim();
    }
    // console.log(des, linkUrl, title, type, id);

    axios({
      method: 'POST',
      url: url + '/ydd_link/update',
      headers: {
        'x-token': token,
      },
      data: {
        des: des,
        img: img,
        title: title,
        type: type,
        url: linkUrl,
        id: id,
      },
    }).then((res) => {
      // console.log(res);
      if (res.data.errno !== 0) return layer.msg('修改失败');
      layer.msg('修改成功');
      if (type == 1) {
        renderText();
      } else {
        renderImg();
      }
    });
  });
});
