let layer = layui.layer;
let form = layui.form;
let laypage = layui.laypage;
var laydate = layui.laydate;


initEditor();
function initEditor() {
  tinymce.init({
    selector: '.content',
    directionality: 'ltr',
    language: 'zh_CN',
    height: '300px',
    statusbar: false,
    width: '1000px',
  });
}

//日期
laydate.render({
  elem: '#date',
});
laydate.render({
  elem: '#date1',
});

//利用时间戳优化时间
function getItem(timer) {
  let date = new Date(timer);
  var y = padZero(date.getFullYear());
  var m = padZero(date.getMonth() + 1);
  var d = padZero(date.getDate());
  let hh = String(date.getHours()).padStart('2',0);
  let mm = String(date.getMinutes()).padStart('2',0);
  let ss = String(date.getSeconds()).padStart('2',0);
  return y + '-' + m + '-' + d + ' ' + hh + ':' + mm + ':' + ss;;
}
function padZero(n) {
  return n > 9 ? n : '0' + n;
}

let q = {
  page: 1,
  limit: 10,
  title: '',
  cateid: '',
  start_time: '',
  end_time: '',
};

renderArt();
filterCate();

//渲染文章
function renderArt() {
  axios({
    method: 'get',
    url: '/ydd_article/list',
    params: q,
  }).then(function (res) {
    // console.log(res);
    let arr = res.data.data.data;
    let count = res.data.data.count;
    let newArr = arr.map((item) => {
      let time = getItem(item.create_date);

      let ishot =
        item.ishot == 0
          ? '<i class="layui-icon layui-icon-star" data-id="' + item.ishot + '" style="font-size: 24px; color: #999999;"></i> '
          : '<i class="layui-icon layui-icon-star-fill" style="font-size: 26px; color: #1E9FFF;"></i> ';

      let istop =
        item.istop == 0
          ? '<i class="layui-icon layui-icon-star" data-id="' + item.istop + '" style="font-size: 24px; color: #999999;"></i> '
          : '<i class="layui-icon layui-icon-star-fill" style="font-size: 26px; color: #1E9FFF;"></i> ';

      let status = item.status == 2 ? '已发布' : '待审核';
      return `
                <tr>
                    <td>${item.id}</td>
                    <td>${item.title}</td>
                    <td>${item.catename}</td>
                    <td>
                        <img src="http://192.168.18.32:8060/${item.pic} " alt="">
                    </td>
                    <td>${time}</td>
                    <td>
                        ${ishot}  
                    </td>
                    <td>
                        ${istop}  
                    </td>
                    <td>
                        <button type="button" class="layui-btn layui-btn-primary layui-border-blue layui-btn-sm" style="border: 1px solid rgb(61, 178, 246);">${status}</button>
                    </td>
                    <td>
                        <button type="button" class="layui-btn  layui-btn-normal layui-btn-xs btn-edit" data-id="${item.id}" style="border-radius:60px; width:30px">
                            <i class="layui-icon layui-icon-edit"></i>   
                        </button>
                        <button type="button" class="layui-btn layui-btn layui-btn-xs layui-btn-danger btn-delete" data-id="${item.id}" style="border-radius:60px; width:30px">
                            <i class="layui-icon layui-icon-delete"></i>   
                        </button>
                    </td>
                </tr>
                `;
    });
    $('tbody').html(newArr.join(''));
    renderPage(count);
  });
}

//渲染分页
function renderPage(count) {
  laypage.render({
    elem: 'pageBox',
    count: count,
    limit: q.limit,
    curr: q.page,
    layout: ['count', 'limit', 'prev', 'page', 'next', 'skip'],
    limits: [2, 5, 10, 20, 30],
    theme: '#1E9FFF',
    jump: function (obj, first) {
      q.limit = obj.limit;
      q.page = obj.curr;

      if (!first) {
        renderArt();
      }
    },
  });
}

//筛选分类
function filterCate() {
  axios({
    method: 'get',
    url: '/ydd_cate/all',
  }).then(function (res) {
    if (res.data.errno === 0) {
      let arr = res.data.data;
      let newArr = arr.map((item) => {
        return `<option value=${item.id}>${item.catename}</option>`;
      });
      newArr.unshift(`<option value="">请选择</option>`);
      $('#select-cate').html(newArr.join(''));
      $('#info-form [name=cateid]').html(newArr.join(''));
      // 通过 layui 重新渲染表单区域的UI结构
      form.render();
    }
  });
}

////为筛选表单绑定 submit 事件
$('#search-form').on('submit', function (e) {
  e.preventDefault();
  let title = $('#search-box').val();
  let cateid = $('#select-cate').val();
  let startDate = $('#test-startDate').val();
  let endDate = $('.test-endDate').val();

  q.title = title;
  q.cateid = cateid;
  q.start_time = new Date(startDate).getTime();
  q.end_time = Date.now(endDate);

  renderArt();
});

//点击重置打开页面
$('#search-form').on('reset', function (e) {
  e.preventDefault();
  $('[name=title]').val('');
  $('[name=catename]').val('');
  $('#test-startDate').val('');
  $('.test-endDate').val('');

  q.title = '';
  q.cateid = '';
  q.start_time = '';
  q.end_time = '';

  renderArt();
});

//导出下载
var idTmr;
function getExplorer() {
  var explorer = window.navigator.userAgent;
  //ie
  if (explorer.indexOf('MSIE') >= 0) {
    return 'ie';
  }
  //firefox
  else if (explorer.indexOf('Firefox') >= 0) {
    return 'Firefox';
  }
  //Chrome
  else if (explorer.indexOf('Chrome') >= 0) {
    return 'Chrome';
  }
  //Opera
  else if (explorer.indexOf('Opera') >= 0) {
    return 'Opera';
  }
  //Safari
  else if (explorer.indexOf('Safari') >= 0) {
    return 'Safari';
  }
}

function method5(tableid) {
  if (getExplorer() == 'ie') {
    var curTbl = document.getElementById(tableid);
    var oXL = new ActiveXObject('Excel.Application');
    var oWB = oXL.Workbooks.Add();
    var xlsheet = oWB.Worksheets(1);
    var sel = document.body.createTextRange();
    sel.moveToElementText(curTbl);
    sel.select();
    sel.execCommand('Copy');
    xlsheet.Paste();
    oXL.Visible = true;

    try {
      var fname = oXL.Application.GetSaveAsFilename('Excel.xls', 'Excel Spreadsheets (*.xls), *.xls');
    } catch (e) {
      print('Nested catch caught ' + e);
    } finally {
      oWB.SaveAs(fname);
      oWB.Close((savechanges = false));
      oXL.Quit();
      oXL = null;
      idTmr = window.setInterval('Cleanup();', 1);
    }
  } else {
    tableToExcel(tableid);
  }
}

function Cleanup() {
  window.clearInterval(idTmr);
  CollectGarbage();
}
var tableToExcel = (function () {
  var uri = 'data:application/vnd.ms-excel;base64,',
    template = '<html><head><meta charset="UTF-8"></head><body><table  border="1">{table}</table></body></html>',
    base64 = function (s) {
      return window.btoa(unescape(encodeURIComponent(s)));
    },
    format = function (s, c) {
      return s.replace(/{(\w+)}/g, function (m, p) {
        return c[p];
      });
    };
  return function (table, name) {
    if (!table.nodeType) table = document.getElementById(table);
    var ctx = {
      worksheet: name || 'Worksheet',
      table: table.innerHTML,
    };
    window.location.href = uri + base64(format(template, ctx));
  };
})();

// 点击跳转新增
$('#btn-add').on('click', function () {
  $('#art-list').fadeOut();
  setTimeout(function () {
    $('#art-manage').fadeIn();
  }, 300);
});

$('#return').on('click', function () {
  $('#art-manage').fadeOut();
  setTimeout(function () {
    $('#art-list').fadeIn();
  }, 300);

  $('#info-form')[0].reset();
  $('.img-box').css('display', 'none');
  $('#image').attr('src', '');
});

//图片
let imgPath;
layui.use('upload', function () {
  var upload = layui.upload;

  //执行实例
  var uploadInst = upload.render({
    elem: '#test1', //绑定元素
    url: 'http://192.168.18.32:8060/admin/common/upload?type=images',
    headers: {
      'x-token': localStorage.getItem('x-token'),
    },
    done: (res) => {
      imgPath = res.data.savePath;
      $('.img-box').css('display', 'block');
      $('#image').attr('src', 'http://192.168.18.32:8060/' + imgPath);
    },
    error: function () {
      //请求异常回调
    },
  });
});

//渲染标签
renderTags();
function renderTags() {
  axios({
    method: 'GET',
    url: '/ydd_tag/all',
  }).then(function (res) {
    let arr = res.data.data;
    let newArr = arr.map((item) => {
      return `
            <input type="checkbox" name="tags" lay-skin="primary" id='${item.id}' title='${item.tagname}' value = ${item.tagname}>
            `;
    });
    $('#taglist').html(newArr.join(''));
    form.render();
  });
}

//推荐和置顶
layui.use('form', function () {
  var form = layui.form;

  //监听提交
  form.on('submit(formDemo)', function (data) {
    let formObj = data.field;

    let num1 = $('#ishot').is(':checked') === true ? 1 : 0;
    let num2 = $('#istop').is(':checked') == true ? 1 : 0;

    let time = Date.now(formObj.create_date);
    let text = tinyMCE.get('content').getContent({ format: 'text' });

    formObj.ishot = num1;
    formObj.istop = num2;
    formObj.create_date = time;
    formObj['pic'] = imgPath;
    formObj['status'] = 2;
    formObj['content'] = text;

    addArt(formObj);

    return false;
  });

  form.on('submit(formDemo2)', function (data) {
    let formObj = data.field;

    let num1 = $('#ishot').is(':checked') === true ? 1 : 0;
    let num2 = $('#istop').is(':checked') == true ? 1 : 0;

    let time = Date.now(formObj.create_date);
    let text = tinyMCE.get('content').getContent({ format: 'text' });

    formObj.ishot = num1;
    formObj.istop = num2;
    formObj.create_date = time;
    formObj['pic'] = imgPath;
    formObj['status'] = 1;
    formObj['content'] = text;

    addArt(formObj);

    return false;
  });
});

//文章添加
function addArt(formObj) {
  axios({
    method: 'post',
    url: '/ydd_article/add',
    data: formObj,
  }).then(function (res) {
    console.log(res);
    if (res.data.errno === 0) {
      layui.layer.msg('添加成功！');
      $('#art-manage').fadeOut();
      setTimeout(function () {
        $('#art-list').fadeIn();
      }, 300);

      renderArt();
    } else {
      layui.layer.msg('添加失败！');
    }
  });
}

//拿到对应文章
let id;
$('tbody').on('click', '.btn-edit', function () {
  id = $(this).attr('data-id');
  $('#art-list').fadeOut();
  setTimeout(function () {
    $('#art-manage').fadeIn();
  }, 300);

  axios({
    method: 'GET',
    url: '/ydd_article/rowInfo',
    params: { id: id },
  }).then(function (res) {
    let { author, cateid, click, content, create_date, description, id, ishot, istop, keywords, lovenum, pic, tags, title } = res.data.data;
    form.val('infoForm', {
      author: author,
      cateid: cateid,
      click: click,
      content: tinyMCE.activeEditor.setContent(content),
      content: content,
      create_date: create_date,
      description: description,
      id: id,
      ishot: ishot,
      istop: istop,
      keywords: keywords,
      lovenum: lovenum,
      pic: pic,
      tags: tags,
      title: title,
    });

    $('.img-box').css('display', 'block');
    $('#image').attr('src', 'http://192.168.18.32:8060/' + pic);

    changeArt();
  });
});

//修改文章
function changeArt() {
  layui.use('form', function () {
    var form = layui.form;

    //监听提交
    form.on('submit(formDemo)', function (data) {
      let formObj = data.field;

      let num1 = formObj.ishot === 'on' ? 1 : 0;
      let num2 = formObj.istop === 'on' ? 1 : 0;
      let time = Date.now(formObj.create_date);
      let text = tinyMCE.get('content').getContent({ format: 'text' });

      formObj.ishot = num1;
      formObj.istop = num2;
      formObj.create_date = time;
      formObj['pic'] = imgPath;
      formObj['status'] = 2;
      formObj['id'] = id;
      formObj['content'] = text;

      change(formObj);
      return false;
    });

    //监听存储
    form.on('submit(formDemo2)', function (data) {
      let formObj = data.field;

      let num1 = formObj.ishot === 'on' ? 1 : 0;
      let num2 = formObj.istop === 'on' ? 1 : 0;
      let time = Date.now(formObj.create_date);
      let text = tinyMCE.get('content').getContent({ format: 'text' });

      formObj.ishot = num1;
      formObj.istop = num2;
      formObj.create_date = time;
      formObj['pic'] = imgPath;
      formObj['status'] = 1;
      formObj['id'] = id;
      formObj['content'] = text;

      change(formObj);
      return false;
    });
  });
}

//修改接口
function change(formObj) {
  axios({
    method: 'post',
    url: '/ydd_article/update',
    data: formObj,
  }).then(function (res) {
    if (res.data.errno === 0) {
      layui.layer.msg('修改文章成功！');
      $('#art-manage').fadeOut();
      setTimeout(function () {
        $('#art-list').fadeIn();
      }, 300);

      $('#info-form')[0].reset();
      $('.img-box').css('display', 'none');
      $('#image').attr('src', '');
      // layui.form.render();
      renderArt();
    } else {
      layui.layer.msg('添加文章失败！');
    }
  });
}

//删除文章
$('tbody').on('click', '.btn-delete', function () {
  let id = $(this).attr('data-id');
  layer.confirm('确定删除吗 ?', { icon: 3, title: '提示' }, function (index) {
    axios({
      method: 'GET',
      url: '/ydd_article/del',
      params: { id: id },
    }).then(function (res) {
      console.log(res);
      if (res.data.errno === 0) {
        layui.layer.msg('删除文章成功！');
        renderArt();
      } else {
        layui.layer.msg('删除文章失败！');
      }
    });
    layer.close(index);
  });
});
