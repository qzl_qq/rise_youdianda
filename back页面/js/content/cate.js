let layer = layui.layer
let form = layui.form
let laypage = layui.laypage;

//登录拿token
axios({
    method: 'POST',
    url: '/index/login',
    data: {
        "login_name": "admin",
        "password": "111111"
    },
}).then(function (res) {
    // 把登录成功的标志位存储在客户端
    let token = res.data.data.token
    localStorage.setItem('x-token', x-token)
})

let q = {
    page: 1,
    limit: 10,
    catename: ''
}
//渲染分类列表
renderCate()
function renderCate() {
    axios({
        method: 'get',
        url: '/ydd_cate/list',
        params: q,
    }).then(function (res) {
        let arr = res.data.data.data
        let count = res.data.data.count
        let newArr = arr.map((item, index) => {
            return `
                <tr>
                    <td>${index + 1}</td>
                    <td class="classID">${item.id}</td>
                    <td class="className">${item.catename}</td>
                    <td class="classIcon">
                        ${item.icon}
                        <i class="fa ${item.icon}"></i>
                    </td>
                    <td class="classSort">${item.sort_num}</td>
                    <td>
                        <button type="button" class="layui-btn  layui-btn-normal layui-btn-xs btn-edit" data-id="${item.id}" style="border-radius:60px; width:30px">
                            <i class="layui-icon layui-icon-edit"></i>   
                        </button>
                        <button type="button" class="layui-btn layui-btn layui-btn-xs layui-btn-danger btn-delete" data-id="${item.id}" style="border-radius:60px; width:30px">
                            <i class="layui-icon layui-icon-delete"></i>   
                        </button>
                    </td>
                </tr>
                `
        })
        $("tbody").html(newArr.join(''))
        renderPage(count)
    })
}

//渲染分页
function renderPage(count) {
    laypage.render({
        elem: 'pageBox',
        count: count, //数据总数，从服务端得到
        limit: q.limit,//每页显示几条数据
        curr: q.page, // 设置默认被选中的分页
        layout: ['count', 'limit', 'prev', 'page', 'next', 'skip',],
        limits: [2, 5, 10, 20, 30],
        theme: '#1E9FFF',
        // 分页发生切换，触发jump回调
        jump: function (obj, first) {
            q.limit = obj.limit
            q.page = obj.curr

            if (!first) {
                renderCate()
            }
        }
    });
}

//根据分类名称搜索
$("#search-form").on('submit', function (e) {
    e.preventDefault()
    let catename = $("[name= catename]").val()
    q.catename = catename
    renderCate()
})

//点击重置打开页面
$("#search-form").on('reset', function (e) {
    e.preventDefault()
    $("[name= catename]").val('')
    q.catename = ''
    renderCate()
})

//新增
let indexAdd
$("#btn-add").on('click', function () {
    indexAdd = layer.open({
        type: 1,
        area: ['500px', '320px'],
        title: '添加分类',
        content: $("#new-add").html()
    });
})

$("body").on('submit', '#form-add', function (e) {
    e.preventDefault()

    axios({
        method: 'post',
        url: '/ydd_cate/add',
        data: {
            catename: $("#form-add [name= catename]").val(),
            icon: $("#form-add [name= icon]").val(),
            sort_num: $("#form-add [name= sort_num]").val(),
        },
    }).then(function (res) {
        if (res.data.errno === 0) {
            renderCate()
            layer.msg('添加成功')
            layer.close(indexAdd)
        } else {
            layer.msg('')
        }
    });
})

// 导出 打印表格
var idTmr;
function getExplorer() {
    var explorer = window.navigator.userAgent;
    //ie  
    if (explorer.indexOf("MSIE") >= 0) {
        return 'ie';
    }
    //firefox  
    else if (explorer.indexOf("Firefox") >= 0) {
        return 'Firefox';
    }
    //Chrome  
    else if (explorer.indexOf("Chrome") >= 0) {
        return 'Chrome';
    }
    //Opera  
    else if (explorer.indexOf("Opera") >= 0) {
        return 'Opera';
    }
    //Safari  
    else if (explorer.indexOf("Safari") >= 0) {
        return 'Safari';
    }
}

function method5(tableid) {
    if (getExplorer() == 'ie') {
        var curTbl = document.getElementById(tableid);
        var oXL = new ActiveXObject("Excel.Application");
        var oWB = oXL.Workbooks.Add();
        var xlsheet = oWB.Worksheets(1);
        var sel = document.body.createTextRange();
        sel.moveToElementText(curTbl);
        sel.select();
        sel.execCommand("Copy");
        xlsheet.Paste();
        oXL.Visible = true;

        try {
            var fname = oXL.Application.GetSaveAsFilename("Excel.xls",
                "Excel Spreadsheets (*.xls), *.xls");
        } catch (e) {
            print("Nested catch caught " + e);
        } finally {
            oWB.SaveAs(fname);
            oWB.Close(savechanges = false);
            oXL.Quit();
            oXL = null;
            idTmr = window.setInterval("Cleanup();", 1);
        }

    } else {
        tableToExcel(tableid)
    }
}

function Cleanup() {
    window.clearInterval(idTmr);
    CollectGarbage();
}
var tableToExcel = (function () {
    var uri = 'data:application/vnd.ms-excel;base64,',
        template = '<html><head><meta charset="UTF-8"></head><body><table  border="1">{table}</table></body></html>',
        base64 = function (
            s) {
            return window.btoa(unescape(encodeURIComponent(s)))
        },
        format = function (s, c) {
            return s.replace(/{(\w+)}/g, function (m, p) {
                return c[p];
            })
        }
    return function (table, name) {
        if (!table.nodeType)
            table = document.getElementById(table)
        var ctx = {
            worksheet: name || 'Worksheet',
            table: table.innerHTML
        }
        window.location.href = uri + base64(format(template, ctx))
    }
})()


//编辑分类
let indexChange
let id
$("tbody").on('click', '.btn-edit', function () {
    id = $(this).attr('data-id')
    indexChange = layer.open({
        type: 1,
        area: ['500px', '320px'],
        title: '添加文章分类',
        content: $("#change-cate").html()
    });
    rowInfo(id)
})

function rowInfo(id) {
    axios({
        method: 'get',
        url: '/ydd_cate/rowInfo',
        params: { id: id }
    }).then(function (res) {
        if (res.data.errno === 0) {
            form.val('formChange', res.data.data)
        }
    })
}

$("body").on('submit', '#form-change', function (e) {
    e.preventDefault()
    axios({
        method: 'post',
        url: '/ydd_cate/update',
        data: {
            id: id,
            catename: $("#form-change [name= catename]").val(),
            icon: $("#form-change [name= icon]").val(),
            sort_num: $("#form-change [name= sort_num]").val(),
        },
    }).then(function (res) {
        console.log(res);
        if (res.data.errno === 0) {
            renderCate()
            layer.close(indexChange)
            layui.layer.msg('修改成功！')
        } else {
            layui.layer.msg('修改失败！')
        }
    });
})

//删除
$("tbody").on('click', '.btn-delete', function () {
    let id_sec = $(this).attr('data-id')
    let len = $('.btn-delete').length
    layer.confirm('确定删除吗 ?', { icon: 3, title: '提示' }, function (index) {
        axios({
            method: 'get',
            url: '/ydd_cate/del',
            params: { id: id_sec }
        }).then(function (res) {
            if (res.data.errno === 0) {
                if (len === 1) {
                    q.pagenum > 1 && q.pagenum--
                }
                renderCate()
                layer.msg('删除用户信息成功！')
            } else {
                layer.msg('系统默认数据，不允许删除')
            }
        })
        layer.close(index);
    })
})

//右边三个按钮
$("#put").on('click', function () {
    $(this).hide()
    $("#down").show()
    $(".layui-card-header").hide()
})

$("#down").on('click', function () {
    $(this).hide()
    $("#put").show()
    $(".layui-card-header").show()
})

$("#refresh").on('click', function () {
    history.go(0)
})

let isTool_BtnTrue = true
$('#cut').on('click', function () {
    // console.log($('.checkbox [name = advposname]').prop('checked'));

    if (isTool_BtnTrue) {
        $('.filter_tooltip').show()
        isTool_BtnTrue = false
    } else {
        $('.filter_tooltip').hide()
        isTool_BtnTrue = true
    }
});
// 封装函数 判断复选框里面的checked的值来决定表格的列是显示还是隐藏
// tableShow(advposname)
// tableShow(advposdesc)
// tableShow(default_data)
// tableShow(advpossize)

$('.checkbox [name = classID]').on('change', function () {
    if ($(this).prop('checked')) {
        $('thead #classID').show()
        $('tbody .classID').show()
    } else {
        $('thead #classID').hide()
        $('tbody .classID').hide()
    }
})

$('.checkbox [name = className]').on('change', function () {
    if ($(this).prop('checked')) {
        $('thead #className').show()
        $('tbody .className').show()
    } else {
        $('thead #className').hide()
        $('tbody .className').hide()
    }
})

$('.checkbox [name = classIcon]').on('change', function () {
    if ($(this).prop('checked')) {
        $('thead #classIcon').show()
        $('tbody .classIcon').show()
    } else {
        $('thead #classIcon').hide()
        $('tbody .classIcon').hide()
    }
})
$('.checkbox [name = classSort]').on('change', function () {
    if ($(this).prop('checked')) {
        $('thead #classSort').show()
        $('tbody .classSort').show()
    } else {
        $('thead #classSort').hide()
        $('tbody .classSort').hide()
    }
})

