$(function () {
  let url = UART;
  let token = localStorage.getItem('x-token');
  let layer = layui.layer;
  let form = layui.form;
  let adv_name;
  let ggw = {
    page: '1', // 页码值，默认请求第一页的数据
    limit: '10', // 每页显示几条数据，默认每页显示2条
    advposdesc: '', // 广告位描述
    advposname: '', // 广告位名称
  };

  // 广告位列表-分页
  let i = 0;
  pageBox();
  function pageBox() {
    axios({
      method: 'GET',
      url: url + '/ydd_advpos/list',
      params: ggw,
      headers: {
        'x-token': token,
      },
    }).then(function (res) {
      // console.log(res);
      let count = res.data.data.count;
      let curr = res.data.data.currentPage;
      page(count, curr);

      adv_name = res.data.data.data;
      let new_arr = res.data.data.data.map((item) => {
        if (item.default_data == null) {
          item.default_data = 0;
        } else {
          item.default_data;
        }
        i++;
        return `
          <tr>
            <td>${item.id}</td>
            <td class="advposname_yc">${item.advposname}</td>
            <td class="advposdesc_yc">${item.advposdesc}</td>
            <td class="default_data_yc">${item.default_data}</td>
            <td class="advpossize_yc">${item.advpossize}</td>
            <td>
            <div class="layui-btn-group" >
            <button type="button"  class="layui-btn layui-btn-sm  layui-btn-radius layui-btn-normal" data-id="${item.id}" id="amend";>
              <i class="layui-icon" >&#xe642;</i>
            </button>
            <button type="button" class="layui-btn layui-btn-sm layui-btn-radius layui-btn-danger btn-delete" data-id="${item.id}">
              <i class="layui-icon">&#xe640;</i>
            </button>
            </div>
            </td>
          </tr> `;
      });
      $('tbody').html(new_arr);
      i = 0;
    });
  }

  // 底部分页
  function page(count, curr) {
    var laypage = layui.laypage;
    //执行一个laypage实例
    laypage.render({
      elem: 'pageBox', //分页容器
      count: count, //数据总数
      limit: ggw.limit, //每页总数
      curr: curr, //页数
      theme: '#27a3ff',
      // // 定义不同功能
      layout: [
        'count' /*条数*/,
        'limit' /*下拉选页数*/,
        'prev' /*上一页*/,
        'page' /*每一页*/,
        'next' /*下一页*/,
        'skip' /*切换分页*/,
      ],
      limits: [2, 4, 6, 8, 10],
      // 每次切换不同的页数都会重新执行渲染函数
      jump: (obj, first) => {
        ggw.page = obj.curr;
        ggw.limit = obj.limit;
        if (!first) {
          pageBox();
        }
      },
    });
  }

  // 广告位删除
  $('tbody').on('click', '.btn-delete', function () {
    let id = $(this).attr('data-id');
    let len = $('.btn-delete').length;
    // console.log(id);
    layer.confirm('确定删除吗，yes or not', { icon: 3, title: '提示' }, function (index) {
      axios({
        method: 'GET',
        url: url + '/ydd_advpos/del',
        params: {
          id: id,
        },
        headers: {
          'x-token': token,
        },
      }).then(function (res) {
        // console.log(res);
        if (res.data.errno === 1000) {
          return layer.msg(res.data.errmsg);
        }
        layer.msg('删除文章成功！');
        if (len === 1) {
          ggw.page = ggw.page = 1 ? 1 : ggw.page - 1;
        }
        pageBox();
      });
      layer.close(index);
    });
  });

  // 搜索模块
  $('#btn_ss').on('click', function (e) {
    e.preventDefault();
    arr = adv_name.filter((item) => item.advposname.includes($('.sousuo').val()));
    // console.log(arr);
    let ss_arr = arr.map((item) => {
      return `
        <tr>
        <td>${item.id}</td>
        <td>${item.advposname}</td>
        <td>${item.advposdesc}</td>
        <td>${item.advpossize}</td>
        <td>${item.advpossize}</td>
        <td>
        <div class="layui-btn-group" >
        <button type="button"  class="layui-btn layui-btn-sm  layui-btn-radius layui-btn-normal" data-id="${item.id}";>
          <i class="layui-icon" >&#xe642;</i>
        </button>
        <button type="button" class="layui-btn layui-btn-sm layui-btn-radius layui-btn-danger btn-delete" data-id="${item.id}" id="amend">
          <i class="layui-icon">&#xe640;</i>
        </button>
      </div>
        </td>
      </tr>
        `;
    });
    $('tbody').html(ss_arr);
  });

  // 重置模块
  $('#btn_cz').on('click', function (e) {
    e.preventDefault();
    pageBox();
    $('.sousuo').val('');
  });

  // 广告位添加
  let Add = null;
  $('.zengjia').on('click', function () {
    Add = layer.open({
      type: 1,
      title: '添加广告位',
      content: $('#dialog-add').html(),
      // , btn: ['确认', '取消'],
      area: ['800px', '350px'],
    });
  });

  // 添加渲染
  $('body').on('submit', '#form-add', function (e) {
    e.preventDefault();
    // console.log($(this).serialize());
    axios({
      method: 'POST',
      url: url + '/ydd_advpos/add',
      data: $(this).serialize(),
      headers: {
        'x-token': token,
      },
    }).then(function (res) {
      // console.log(res);
      if (res.status !== 200) {
        return layui.layer.msg('添加失败！');
      }
      layer.msg('添加成功');
      pageBox();
      layer.close(Add);
    });
  });
  // 点击取消
  $('body').on('click', '.quxiao', function () {
    layer.close(Add);
  });

  // 广告位修改
  let amend = null;
  let index_amend;
  $('tbody').on('click', '#amend', function () {
    index_amend = $(this).attr('data-id');
    // console.log(index_amend);
    amend = layer.open({
      type: 1,
      title: '修改广告位',
      content: $('#dialog-amend').html(),
      // , btn: ['确认', '取消'],
      area: ['800px', '350px'],
    });

    // 点击修改获得当前表格里内容
    axios({
      method: 'GET',
      url: url + '/ydd_advpos/rowInfo',
      params: {
        id: index_amend,
      },
      headers: {
        'x-token': token,
      },
    }).then(function (res) {
      // console.log(res);
      form.val('form-edit', res.data.data);
    });
  });

  // 提交修改
  $('body').on('submit', '#form-amend', function (e) {
    e.preventDefault();
    // let p = $(this).serialize()
    let advposname = $('[name=advposname]').val();
    let advposdesc = $('[name=advposdesc]').val();
    let advpossize = $('[name=advpossize]').val();
    axios({
      method: 'POST',
      url: url + '/ydd_advpos/update',
      data: {
        advposname: advposname,
        advposdesc: advposdesc,
        advpossize: advpossize,
        id: index_amend,
      },
      headers: {
        'x-token': token,
      },
    }).then(function (res) {
      if (res.status !== 200) {
        return layui.layer.msg('修改失败！');
      }
      // console.log(res);
      layer.msg('修改成功');
      pageBox();
      layer.close(amend);
    });
  });

  // 右上角的功能
  let a = true;
  $('.rigth_one').on('click', function () {
    if (a) {
      $('#adv_top').hide();
      $(this).css('transform', 'rotate(180deg)');
      a = false;
      return;
    }
    // console.log(12);
    $('#adv_top').show();
    $(this).css('transform', 'rotate(360deg)');
    a = true;
  });

  $('.check').prop('checked', true);
  let btns = document.querySelectorAll('.check');
  for (let i = 0; i < btns.length; i++) {
    btns[i].setAttribute('index', i);
    btns[i].onclick = function () {
      index = this.getAttribute('index');
      console.log(index);
      if (index == 0) {
        $('.advposname_yc').toggleClass('all');
      } else if (index == 1) {
        $('.advposdesc_yc').toggleClass('all');
      } else if (index == 2) {
        $('.default_data_yc').toggleClass('all');
      } else if (index == 3) {
        $('.advpossize_yc').toggleClass('all');
      }
    };
  }

  let b = true;
  $('.layui-icon-app').on('click', function () {
    if (b) {
      $('.adv-box').show();
      b = false;
    } else {
      $('.adv-box').hide();
      b = true;
    }
  });
  // 重新渲染
  $('#reload').click(function () {
    pageBox();
  });
  // 导出文件
  $('#export').click(function (e) {
    var table = $('.layui-table');
    var preserveColors = table.hasClass('table2excel_with_colors') ? true : false;
    $(table).table2excel({
      exclude: '.noExl',
      name: 'Excel Document Name',
      filename: 'myFileName' + new Date().toISOString().replace(/[\-\:\.]/g, '') + '.xls',
      fileext: '.xls',
      exclude_img: true,
      exclude_links: true,
      exclude_inputs: true,
      preserveColors: preserveColors,
    });
  });
});
