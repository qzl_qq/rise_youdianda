$(function () {
  localStorage.removeItem('x-token');

  let url = UART;
  let layer = layui.layer;
  let form = layui.form;
  // 获取注册的用户token信息
  let token = localStorage.getItem('token');
  // console.log(token);

  // 给表单添加submit提交事件
  $('.layui-form').submit(function (e) {
    // 阻止默认跳转
    e.preventDefault();
    // 获取输入的数据
    let login_name = $('input[name="username"]').val().trim();
    let password = $('input[name="password"]').val().trim();
    console.log(login_name, password);

    // 提交登录接口
    axios({
      method: 'POST',
      url: url + '/index/login',
      headers: {
        token,
      },
      data: { login_name, password },
    }).then((res) => {
      console.log(res);
      if (res.status !== 200) return false;
      if (res.data.errmsg === '账号不正确' || res.data.errmsg === '密码不正确') {
        return layer.msg(res.data.errmsg);
      }
      layer.msg('登录成功');
      localStorage.setItem('x-token', res.data.data.token);
      // 跳转到主页
      setTimeout(() => {
        location.href = './index-主页.html';
      });
    });
  });
});
