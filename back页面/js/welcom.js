// 定义token
let token = localStorage.getItem('x-token');
// console.log(token);
getData();
// 请求数据
function getData() {
  axios({
    method: 'get',
    url: UART + '/index/baseInfo',
    params: {},
    headers: {
      'x-token': token,
    },
  }).then(function (res) {
    // console.log(res);
    // 接受数据
    let data = res.data.data;
    // 获取用户性别，通过对象的方式进行传递
    let user_woman = res.data.data.user.user_woman;
    let user_man = res.data.data.user.user_man;
    Gender({ user_woman, user_man });
    // 调用函数
    admin(data);
    registered(data);
    like(data);
    release(data);
  });
}

// 渲染用户
function admin(data) {
  $('.admin_num').html(`<p>管理用户<br><b>${data.admin_num}</b>名</p>`);
  $('.admin_user').html(`<p>平台注册用户<br><b>${data.user.total}</b>名</p>`);
  $('.article_num').html(`<p>平台文章总数<br><b>${data.article_num}</b>篇</p>`);
  $('.advpos_num').html(` <p>广告位<b>${data.advpos_num}</b>个</p> <p>广告图<b>${data.advimg_num}</b>张</p>`);
}

//渲染注册用户
function registered(data) {
  let reg = '';
  data.new_user.forEach((item) => {
    reg += `<tr>
      <td><span>${item.username}</span><span>${item.sex === 1 ? '女' : '男'}</span><span>${item.address !== null ? item.address : ''}</span></td>
    </tr>
      `;
  });
  $('.registered tbody').html(reg);
}

// 点赞
function like(data) {
  let reg = '';
  data.hot_article.forEach((item) => {
    reg += `<tr>
      <td>${item.title}</td>
    </tr>
      `;
  });
  $('.like tbody').html(reg);
}

// 性别
function Gender(data) {
  let myChart = echarts.init(document.querySelector('.gender tbody td'));
  let option = {
    color: ['#94cb74', '#5473c6'],
    tooltip: {
      trigger: 'item',
    },
    legend: {
      top: '5%',
      left: 'center',
    },
    series: [
      {
        name: 'Access From',
        type: 'pie',
        radius: ['40%', '70%'],
        avoidLabelOverlap: false,
        itemStyle: {
          borderRadius: 10,
          borderColor: '#fff',
          borderWidth: 2,
        },
        label: {
          show: false,
          position: 'center',
        },
        emphasis: {
          label: {
            show: true,
            fontSize: '40',
            fontWeight: 'bold',
          },
        },
        labelLine: {
          show: false,
        },
        data: [
          { value: data.user_woman, name: '女性' },
          { value: data.user_man, name: '男性' },
        ],
      },
    ],
  };
  // 3. 配置项和数据给我们的实例化对象
  myChart.setOption(option);
}
// 柱状图
function release(data) {
  let chart = data.all_cate.map(function (item) {
    return item.catename;
  });
  let dataList = data.all_cate.map(function (item) {
    return item.num;
  });

  let myChart = echarts.init(document.querySelector('.release'));
  let option = {
    color: ['#5473c6'],
    xAxis: {
      type: 'category',
      data: chart,
    },
    yAxis: {
      type: 'value',
    },
    series: [
      {
        data: dataList,
        type: 'bar',
        showBackground: true,
        backgroundStyle: {
          color: 'rgba(180, 180, 180, 0.2)',
        },
      },
    ],
  };
  // 3. 配置项和数据给我们的实例化对象
  myChart.setOption(option);
}
